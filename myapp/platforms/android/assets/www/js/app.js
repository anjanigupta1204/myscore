// Ionic Starter App
   var db=null;
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ion-datetime-picker','starter.sqliteservices', 'ngCordova'])

.run(function($ionicPlatform,$rootScope,$cordovaSQLite,$ionicHistory,$ionicNavBarDelegate) {

  $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
              cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
              cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
              // org.apache.cordova.statusbar required
              StatusBar.styleDefault();
            }

            $rootScope.dateValue = "dd-mm-yyyy";
            $rootScope.timeValue = new Date();
            $rootScope.datetimeValue = new Date();
          // define some random object
          $rootScope.bigData = {};
          // COLLAPSE =====================
          $rootScope.isCollapsed = false;

          db = window.openDatabase("myscore.db", "1.0", "MyScore", -1);


  });
        $ionicPlatform.registerBackButtonAction(function (event) {
            //if specified state matches else go back
            //alert($ionicHistory.currentStateName())
           if ($ionicHistory.currentStateName() === 'app.dashboard'||$ionicHistory.currentStateName() === 'login'){
          navigator.app.exitApp();
           } else {
            $ionicHistory.goBack();
          }
        }, 100);
		
	
    $rootScope.$on('$ionicView.enter', function() {
		// code to run each time view is entered
		$ionicNavBarDelegate.showBar(true);
	});
	
	
	
})
.directive('ionToggleText', function () {

  var $ = angular.element;

  return {
    restrict: 'A',
    link: function ($scope, $element, $attrs) {

      // Try to figure out what text values we're going to use

      var textOn = $attrs.ngTrueValue || 'Yes',
        textOff = $attrs.ngFalseValue || 'No';

      if ($attrs.ionToggleText) {
        var x = $attrs.ionToggleText.split(';');

        if (x.length === 2) {
          textOn = x[0] || textOn;
          textOff = x[1] || textOff;
        }
      }

      // Create the text elements

      var $handleTrue = $('<div class="handle-text handle-text-true">' + textOn + '</div>'),
        $handleFalse = $('<div class="handle-text handle-text-false">' + textOff + '</div>');
      var label = $element.find('label');
      if (label.length) {
        label.addClass('toggle-text');
        // Locate both the track and handle elements
        var $divs = label.find('div'),
          $track, $handle;

        angular.forEach($divs, function (div) {
          var $div = $(div);

          if ($div.hasClass('handle')) {
            $handle = $div;
          } else if ($div.hasClass('track')) {
            $track = $div;
          }
        });

        if ($handle && $track) {

          // Append the text elements

          $handle.append($handleTrue);
          $handle.append($handleFalse);

          // Grab the width of the elements

          var wTrue = $handleTrue[0].offsetWidth,
            wFalse = $handleFalse[0].offsetWidth;

          // Adjust the offset of the left element

          $handleTrue.css('left', '-' + (wTrue + 10) + 'px');

          // Ensure that the track element fits the largest text

          var wTrack = Math.max(wTrue, wFalse);
          $track.css('width', (wTrack + 60) + 'px');

        }
      }
    }
  };

})
.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    .state('app', {
	   url: '/app',
	   abstract: true,
	   templateUrl: 'templates/menu.html',
	   controller:'menuCtrl',
	   cache: false
	})
    .state('app.dashboard', {
	   url: '/dashboard',
      views: {
        'menuContent': {
        templateUrl: 'templates/dashboard.html',
        controller:'dashboardCtrl',
	   cache: false
        }
      }
	})
    .state('app.guideLine', {
		  url: '/guideLine',
		  views: {
		  'menuContent': {
			templateUrl: 'templates/guideline.html',
			controller:'guideLineCtrl',
	   cache: false
		  }
		}
	})
    .state('app.salesData', {
		url: '/salesData',
		  views: {
		  'menuContent': {
			templateUrl: 'templates/salesdata.html',
			controller: 'salesDataCtrl',
	   cache: false
		  }
		}
	})
	.state('app.salesNorms', {
	   url: '/salesNorms',
		  views: {
		  'menuContent': {
			templateUrl: 'templates/salesnorms.html',
			controller: 'salesNormsCtrl',
	   cache: false
		  }
		}
	})
	.state('app.survey', {
	   url: '/survey',
		  views: {
		  'menuContent': {
			templateUrl: 'templates/survey.html',
			controller: 'surveyQuestions',
	   cache: false
		  }
		}
	})
	.state('app.remark', {
	   url: '/remark',
		  views: {
		  'menuContent': {
			templateUrl: 'templates/remark.html',
			controller: 'remark',
	   cache: false
		  }
		}
	})
   .state('login', {
             url: '/login',
             templateUrl: 'templates/login.html',
             controller:'loginCtrl',
	   cache: false
       })
	.state('app.reports', {
			  url: '/reports',
			  views: {
			  'menuContent': {
				templateUrl: 'templates/pendingReports.html',
				controller: 'reportsCtrl',
	   cache: false
			  }
			}
		})
	.state('app.profile', {
			url: '/profile',
			  views: {
			  'menuContent': {
				templateUrl: 'templates/profile.html',
				controller: 'profileCtrl',
	   cache: false
			  }
			}
		})
		/* .state('logout', {
    			url: '/logout',
    			controller: 'logoutCtrl'
    		}) */
	.state('app.mapping', {
	   url: '/mapping',
		views: {
		  'menuContent': {
		  templateUrl: 'templates/mapping.html' ,
		  controller:'mappingCtrl',
	   cache: false
		  }
		}
	});
	//alert(localStorage.getItem("isLoggedIn"))
/*	if(localStorage.getItem("isLoggedIn")=='true'){
	//alert('if')
	  $urlRouterProvider.otherwise('/app/dashboard');
	}else{
		//alert('else')
    $urlRouterProvider.otherwise('/login');
	}*/
	 $urlRouterProvider.otherwise('/login');


})
.filter('num', function() {
    return function(input) {
      return parseInt(input, 10);
    };
});
