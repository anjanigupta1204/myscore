angular.module('starter.controllers', [])
/**
 @Name: loginCtrl
 @Type: Controller
 @Author: Anjani Kr Gupta
 @Date: 4-Jul-2016
 */
.controller('loginCtrl', function ($scope, $http, $state, $ionicLoading, userAuthenticationService, $cordovaInAppBrowser, $cordovaSQLite, userProfile) {

	//redirect to dashboard if remember me
	//alert(localStorage.getItem("LoginUser"))
	//alert(localStorage.getItem("password"))
	//alert(localStorage.getItem("rememberMe"))
	if (localStorage.getItem("LoginUser") != null && localStorage.getItem("password") != null && localStorage.getItem('rememberMe')=='true') {
		$state.go('app.dashboard');
	}

	var user = {};
	$scope.email = "";
	$scope.password = "";
	$scope.errorMsgLogin = "";
	$scope.isChecked = 'false';

	$scope.rememberMe = function (isChecked) {
		$scope.isChecked = isChecked;
	}

	$scope.login = function () {

		if(checkNetworkConnection()){
				if($scope.email!="" && $scope.password!=""){
							  $ionicLoading.show();
							  var url = BASE_URL + 'login';
							  var userData = {username: $scope.email, password: $scope.password};
							  userAuthenticationService.login(url, userData).then(function (data) {
								$ionicLoading.hide();
								if (data != undefined || data != null) {
								  if(data.status){
									  if(data.jsonData.empDesignation=="tsm"){
										  var user = {};
										  user.username = $scope.email;
										  user.password = CryptoJS.MD5($scope.password).toString();
										  user.emp_code = data.jsonData.empCode;
										  user.emp_designation = data.jsonData.empDesignation;
										  user.userId = data.jsonData.userId;
										  //console.log(user)
										  //alert(JSON.stringify(res))
										  userProfile.saveUser(user).then(function(res){
										  //alert(JSON.stringify(res))
												localStorage.setItem("loginUserDesignation", data.jsonData.empDesignation);
												localStorage.setItem('LoginUser', data.jsonData.userName);
												localStorage.setItem('password', $scope.password);
												localStorage.setItem('isLoggedIn', true);
												localStorage.setItem('USER_ID', data.jsonData.userId);
												localStorage.setItem('USER_DESIGNATION', data.jsonData.empDesignation);
												localStorage.setItem('rememberMe', $scope.isChecked);

												//for asm mapping
												localStorage.setItem('empCode', data.jsonData.empCode);
												localStorage.setItem('department', data.jsonData.empDepartment);
												$state.go('app.dashboard');
										});
									  }else{
										  $scope.errorMsgLogin = "Currently only for TSM.";
									  }

								  }else{
									$scope.errorMsgLogin = data.message;
								  }
								} else {
								  //Close loader
								  $ionicLoading.hide();
								}
							  }).catch(function (data) {
								$scope.errorMsgLogin = 'Server error.';
								$ionicLoading.hide();
							  });
					}else{
						  $scope.errorMsgLogin = "Please enter login credentials.";
					}
		}else{
			/* if (localStorage.getItem("LoginUser") != null && localStorage.getItem("password") != null && localStorage.getItem('rememberMe')=='true') {
				$state.go('app.dashboard');
			}else{

				alert("Sorry, no Internet connectivity detected. Please reconnect and try again.");

			} */
			  var user = {};
			  user.username = $scope.email;
			  user.password = CryptoJS.MD5($scope.password).toString();
				userProfile.login(user).then(function(res){
					//console.log(res)
					//alert(JSON.stringify(res))
					if(res!=undefined && res.rows.length>0){
						localStorage.setItem("loginUserDesignation", res.rows[0].emp_designation);
						localStorage.setItem('LoginUser', res.rows[0].username);
						localStorage.setItem('password', $scope.password);
						localStorage.setItem('isLoggedIn', true);
						localStorage.setItem('USER_ID', res.rows[0].user_id);
						localStorage.setItem('USER_DESIGNATION', res.rows[0].emp_designation);
						localStorage.setItem('rememberMe', $scope.isChecked);
						$state.go('app.dashboard');
					}else{
						$scope.errorMsgLogin = "Sorry, no Internet connectivity detected. Please reconnect and try again.";
					}
				});


		}


	}

})
/**
 @Name: dashboardCtrl
 @Type: Controller
 @Author: Anjani Kr Gupta
 @Description:
 @Date: 4-Jul-2016
 */
.controller('dashboardCtrl', function ( $ionicHistory, $location, $ionicNavBarDelegate, $ionicSideMenuDelegate, $cordovaInAppBrowser, $scope, $http, $state, $ionicLoading, getDealerService, $cordovaSQLite, DEALERS, survey, createSurvey, $ionicPopup) {

	 /* $ionicHistory.clearHistory();
	 $ionicHistory.clearCache();
	 $ionicHistory.removeBackView(); */
	if(localStorage.getItem('USER_ID')=="" || localStorage.getItem('USER_ID')==null){
		$state.go('login');
	}else{

    $scope.menu = true;
	$scope.showHide = false;
	$scope.tsmStatus = false;
	$scope.asmStatus = false;
	$scope.zoStatus = false;
	$scope.getLastThreeMonth = getLastThreeMonth();
	$scope.getLastThreeYear = getLastThreeYear();

	var d = new Date();
	var currentMonth = d.getMonth()+1;
	var currentYear = d.getFullYear();
	$scope.currentYear= currentYear;
	$scope.currentMonth= currentMonth;
	////console.log(currentMonth)
	localStorage.setItem("filterMonth", currentMonth);
	localStorage.setItem("filterYear", currentYear);
	localStorage.setItem("filterDealerCode", "-1");

	var response = [];
	var month = localStorage.getItem("filterMonth");
	var year = localStorage.getItem("filterYear");
	//check internet connection
	//alert(checkNetworkConnection())
	//$cordovaSQLite.execute(db, "DELETE FROM dealers");
	var stats = {};
	stats.loggedInEmpDesignation = localStorage.getItem('USER_DESIGNATION');
	if(checkNetworkConnection()){
	//alert("Internet")

		var url = BASE_URL + 'dealerList?userId=' + localStorage.getItem('USER_ID') + '&month=' + month + '&year=' + year;
		//console.log(url)
		$ionicLoading.show();

		getDealerService.getDealers(url, "").then(function (dataIncoming) {
			//console.log(dataIncoming)

			if (dataIncoming != undefined || dataIncoming != null) {

				if(dataIncoming.status){
					var data = dataIncoming.jsonData.dealerData;
					//console.log(data);


				stats.empId = localStorage.getItem('USER_ID');
				stats.empCode = dataIncoming.jsonData.stats.empCode;
				//stats.empDesignation = dataIncoming.jsonData.stats.empDesignation;

				for (var i = 0; i < data.length; i++)
				{
					DEALERS.add(data[i],stats);
				}

				localStorage.setItem('empCode',stats.empCode);
				localStorage.setItem('empDesignation',stats.empDesignation);

				stats.completed = dataIncoming.jsonData.stats.completed.completedSurvey;
				stats.pending = dataIncoming.jsonData.stats.pending.pendingSurvey;

        if(stats.loggedInEmpDesignation=='tsm'){
            var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+ " AND year="+localStorage.getItem("filterYear")+" AND tsmId="+localStorage.getItem('USER_ID');
            $scope.tsmStatus = true;
          }else if(stats.loggedInEmpDesignation=='asm'){
            var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+ " AND year="+localStorage.getItem("filterYear")+" AND asmId="+localStorage.getItem('USER_ID');
            $scope.asmStatus = true;
          }else if(stats.loggedInEmpDesignation=='zo'){
            var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+ " AND year="+localStorage.getItem("filterYear")+" AND zoId="+localStorage.getItem('USER_ID');
            $scope.zoStatus = true;
          }else{
            var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+ " AND year="+localStorage.getItem("filterYear")+" AND tsmId="+localStorage.getItem('USER_ID');
            $scope.tsmStatus = true;
          }

				DEALERS.addStats(stats).then(function(res){
					$scope.completed = dataIncoming.jsonData.stats.completed.completedSurvey;
					$scope.pending = dataIncoming.jsonData.stats.pending.pendingSurvey;
					$scope.month = data[0]['month'];
					$scope.monthText = data[0]['monthText'];
					$scope.year = data[0]['year'];

					/*if(stats.empDesignation=='tsm'){
            var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+ " AND year="+localStorage.getItem("filterYear")+" AND tsmId="+localStorage.getItem('USER_ID');
          }else if(stats.empDesignation=='asm'){
            var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+ " AND year="+localStorage.getItem("filterYear")+" AND asmId="+localStorage.getItem('USER_ID');
          }else if(stats.empDesignation=='zo'){
            var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+ " AND year="+localStorage.getItem("filterYear")+" AND zoId="+localStorage.getItem('USER_ID');
          }else{
            var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+ " AND year="+localStorage.getItem("filterYear")+" AND tsmId="+localStorage.getItem('USER_ID');
          }*/

						$cordovaSQLite.execute(db, query).then(function (result) {
							////console.log(result);
							for (var i = 0; i < result.rows.length; i++) {
								response.push(result.rows[i]);
							}
							$state.go('app.dashboard');
							////console.log(response)
							$scope.dealersData = response;
							$scope.dealersDataList = response;
							$ionicLoading.hide();
						}, function (err) {
							console.error(err);
						});
				});

				}else{
					$ionicLoading.hide();
					$scope.errorMsg = dataIncoming.message;
				}
			} else {
				//Close loader
				$scope.formLoader = false;
				$ionicLoading.hide();
			}
		}).catch(function (data) {
			alert(data)
			$ionicLoading.hide();
		});
	}else{
	//alert("No Internet")
/* 			var monthNames = [ "January", "February", "March", "April", "May", "June",
	"July", "August", "September", "October", "November", "December" ]; */
	////console.log(localStorage.getItem('filterMonth'));
        if(stats.loggedInEmpDesignation=='tsm'){
            var query = "SELECT * from dealers d INNER JOIN survey_stats st ON d.tsmId=st.loggedInEmpId WHERE d.month="+localStorage.getItem('filterMonth')+" AND d.year="+localStorage.getItem('filterYear');
            $scope.tsmStatus = true;
          }else if(stats.loggedInEmpDesignation=='asm'){
            var query = "SELECT * from dealers d INNER JOIN survey_stats st ON d.asmId=st.loggedInEmpId WHERE d.month="+localStorage.getItem('filterMonth')+" AND d.year="+localStorage.getItem('filterYear');
            $scope.asmStatus = true;
          }else if(stats.loggedInEmpDesignation=='zo'){
            var query = "SELECT * from dealers d INNER JOIN survey_stats st ON d.zoId=st.loggedInEmpId WHERE d.month="+localStorage.getItem('filterMonth')+" AND d.year="+localStorage.getItem('filterYear');
            $scope.zoStatus = true;
          }else{
            var query = "SELECT * from dealers d INNER JOIN survey_stats st ON d.tsmId=st.loggedInEmpId WHERE d.month="+localStorage.getItem('filterMonth')+" AND d.year="+localStorage.getItem('filterYear');
            $scope.tsmStatus = true;
          }
		//var query = "SELECT * from dealers d INNER JOIN survey_stats st ON d.asmId=st.loggedInEmpId WHERE d.month="+localStorage.getItem('filterMonth')+" AND d.year="+localStorage.getItem('filterYear');
		$cordovaSQLite.execute(db, query).then(function (result) {
			//console.log(result);
			if(result.rows.length>0){
				for (var i = 0; i < result.rows.length; i++) {
					response.push(result.rows[i]);
				}
				localStorage.setItem('empCode',result.rows[0].empCode);
				$state.go('app.dashboard');
				$scope.dealersData = response;
				$scope.dealersDataList = response;

				$scope.completed = result.rows[0].completed;
				$scope.pending = result.rows[0].pending;
				$scope.month = result.rows[0].month;
				$scope.monthText = monthNames[month-1];
				$scope.year = result.rows[0].year;
				$ionicLoading.hide();
			}else{
				alert("Sorry, no Internet connectivity detected. Please reconnect and try again.");
				$state.go('app.dashboard');
			}
		}, function (err) {
			console.error(err);
		});
	}

}
	$scope.searchFilter = function () {
		$scope.showHide = !$scope.showHide;
	}
	/*
	* Method: start
	* Description: start survey
	*/
	$scope.start = function (survey_status, dealerCode, dealerName, dealerMappedStaffDepartment, month, year, surveyId, empCode, referenceId) {

	//	//console.log(surveyId)
	//	//console.log(referenceId)
	//alert(survey_status);
		localStorage.setItem("dealerCode",dealerCode);
		localStorage.setItem("dealerName",dealerName);
		localStorage.setItem("department",dealerMappedStaffDepartment);
		localStorage.setItem("month",month);
		localStorage.setItem("year",year);
		localStorage.setItem("surveyId",surveyId);
		localStorage.setItem("referenceId",referenceId);
		localStorage.setItem('survey_status',formStatus(survey_status).status);

		//alert(localStorage.getItem('survey_status'));
			localStorage.setItem('btnTxt',formStatus(survey_status).btnTxt);
				localStorage.setItem('btnStatus',formStatus(survey_status).btnStatus);

		if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
			var info = surveyBasicInfo(dealerCode, dealerName, dealerMappedStaffDepartment, month, year, surveyId, empCode, referenceId);
			//alert(JSON.stringify(info))
			createSurvey.submitSurveyBasicInfo(info).then(function (res) {
			  ////console.log(res)
			  //alert(JSON.stringify(res))
			  createSurvey.updateSurveyStatusOnStart(info).then(function(d){

				$state.go('app.guideLine');
			  });
			});
		}else{
			   $state.go('app.guideLine');
		}
	}
	$scope.search = function ($key) {
		////console.log($key)
		var response = [];
		$ionicLoading.show();
		if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
      var query = "SELECT * FROM dealers WHERE dealerName LIKE '%"+$key+"%' AND month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND tsmId="+localStorage.getItem('USER_ID');
		   $scope.tsmStatus = true;
		}else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
       var query = "SELECT * FROM dealers WHERE dealerName LIKE '%"+$key+"%' AND month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND asmId="+localStorage.getItem('USER_ID');
		     $scope.asmStatus = true;
		}else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
      var query = "SELECT * FROM dealers WHERE dealerName LIKE '%"+$key+"%' AND month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND zoId="+localStorage.getItem('USER_ID');
		   $scope.zoStatus = true;
		}else{
      var query = "SELECT * FROM dealers WHERE dealerName LIKE '%"+$key+"%' AND month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND tsmId="+localStorage.getItem('USER_ID');
		   $scope.tsmStatus = true;
		}

		$cordovaSQLite.execute(db, query).then(function (result) {
			//console.log(result.rows.length);
			for (var i = 0; i < result.rows.length; i++) {
				response.push(result.rows[i]);
			}
			$scope.dealersData = response;
			//console.log(response);
			$ionicLoading.hide();
		}, function (err) {
			console.error(err);
		});
	}

	$scope.filterByDealer = function(code){
		localStorage.setItem("filterDealerCode", code);
		var month = localStorage.getItem('filterMonth');
		var year = localStorage.getItem('filterYear');
/* 		//console.log(localStorage.getItem('filterMonth'))
		//console.log(localStorage.getItem('filterYear'))
		//console.log(code) */
		//var searchByRefId = generateReferenceId(localStorage.getItem('filterMonth'), localStorage.getItem('filterYear'), code, localStorage.getItem('empCode'));
		var stats = {};
    stats.loggedInEmpDesignation = localStorage.getItem('USER_DESIGNATION');
    stats.empId = localStorage.getItem('USER_ID');

		if(checkNetworkConnection()){
			$ionicLoading.show();
      if(code!=-1){
          if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
            var query = "SELECT * from dealers WHERE dealerCode="+code+" AND month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND tsmId="+localStorage.getItem('USER_ID');
            $scope.tsmStatus = true;
          }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
            $scope.asmStatus = true;
            var query = "SELECT * from dealers WHERE dealerCode="+code+" AND month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND asmId="+localStorage.getItem('USER_ID');
          }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
            $scope.zoStatus = true;
            var query = "SELECT * from dealers WHERE dealerCode="+code+" AND month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND zoId="+localStorage.getItem('USER_ID');
          }else{
            $scope.tsmStatus = true;
            var query = "SELECT * from dealers WHERE dealerCode="+code+" AND month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND tsmId="+localStorage.getItem('USER_ID');
          }
      }else{
        if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND tsmId="+localStorage.getItem('USER_ID');
        		    $scope.tsmStatus = true;
        			}else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
        				$scope.asmStatus = true;
                var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND asmId="+localStorage.getItem('USER_ID');
        			}else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
        			  $scope.zoStatus = true;
                var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND zoId="+localStorage.getItem('USER_ID');
              }else{
                $scope.tsmStatus = true;
                var query = "SELECT * from dealers WHERE month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear")+" AND tsmId="+localStorage.getItem('USER_ID');
              }
      }



      $cordovaSQLite.execute(db, query).then(function (result) {
            $empCodeLinkedToTsm = result.rows[0].empCode;
            var searchByRefId = generateReferenceId(localStorage.getItem('filterMonth'), localStorage.getItem('filterYear'), code, $empCodeLinkedToTsm);
            var query = "SELECT * from dealers WHERE dealerCode="+code+" AND referenceId="+searchByRefId+ " AND month="+localStorage.getItem("filterMonth")+" AND year="+localStorage.getItem("filterYear");
            				$cordovaSQLite.execute(db, query).then(function (result) {
            					////console.log(result);
            					var response = [];
            					if(result.rows.length>0){

            						for (var i = 0; i < result.rows.length; i++) {
            							response.push(result.rows[i]);
            						}
            						$state.go('app.dashboard');
            						$scope.dealersData = response;
            						//$scope.dealersDataList = response;
            						$ionicLoading.hide();
            					}else{
            						var url = BASE_URL + 'filterDealerList?userId=' + localStorage.getItem('USER_ID') + '&month=' + localStorage.getItem('filterMonth') + '&year=' + localStorage.getItem('filterYear') + '&dealerCode=' + code;
            							getDealerService.getDealers(url, "").then(function (dataIncoming) {
            								////console.log(dataIncoming.jsonData)
            								stats.empCode = dataIncoming.jsonData.stats.empCode;
            								var data = dataIncoming.jsonData.dealerData;

            								if (data != undefined || data != null) {

            									for (var i = 0; i < data.length; i++)
            									{
            										//console.log("add dealer");
            										DEALERS.add(data[i],stats);
            									}
            										$scope.month = data[0]['month'];
            										$scope.monthText = data[0]['monthText'];
            										$scope.year = data[0]['year'];

            										 if(code!=-1){
                                      if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                                         var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+code+" AND tsmId="+localStorage.getItem('USER_ID');
                                         $scope.tsmStatus = true;
                                       }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                                         $scope.asmStatus = true;
                                         var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+code+" AND asmId="+localStorage.getItem('USER_ID');
                                       }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                                         $scope.zoStatus = true;
                                         var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+code+" AND zoId="+localStorage.getItem('USER_ID');
                                       }else{
                                         $scope.tsmStatus = true;
                                         var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+code+" AND tsmId="+localStorage.getItem('USER_ID');
                                       }
                                }else{
                                     if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                                       var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND tsmId="+localStorage.getItem('USER_ID');
                                       $scope.tsmStatus = true;
                                     }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                                       $scope.asmStatus = true;
                                       var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND asmId="+localStorage.getItem('USER_ID');
                                     }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                                       $scope.zoStatus = true;
                                       var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND zoId="+localStorage.getItem('USER_ID');
                                     }else{
                                       $scope.tsmStatus = true;
                                       var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND tsmId="+localStorage.getItem('USER_ID');
                                     }
                                    // var query = "SELECT * from dealers WHERE month="+month+" AND year="+year;
                                }
                                ////console.log(query)
                                  $cordovaSQLite.execute(db, query).then(function (result) {
                                    //console.log(result);
                                    for (var i = 0; i < result.rows.length; i++) {
                                      response.push(result.rows[i]);
                                    }

                                    ////console.log(response)
                                     $scope.dealersData = response;
                                   // $scope.dealersDataList = response;
                                    $ionicLoading.hide();
                                    $state.go('app.dashboard');
                                  }, function (err) {
                                    console.error(err);
                                  });

            								} else {
            									//Close loader
            									$scope.formLoader = false;
            									$ionicLoading.hide();
            								}
            							}).catch(function (data) {
            								alert(data)
            								$ionicLoading.hide();
            							});
            					}
            				}, function (err) {
            					console.error(err);
            				});
      }, function (err) {
            console.error(err);
       });

		}else{
			var response = [];
			$ionicLoading.show();

			var dealerCode = localStorage.getItem("filterDealerCode");
			var month = localStorage.getItem("filterMonth");
			var year = localStorage.getItem("filterYear");

			if(dealerCode!=-1){
          if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
            var query = "SELECT * FROM dealers WHERE dealerCode ='"+dealerCode+"' AND month='"+month+"' AND year='"+year+"' AND tsmId="+localStorage.getItem('USER_ID');
            $scope.tsmStatus = true;
          }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
            $scope.asmStatus = true;
            var query = "SELECT * FROM dealers WHERE dealerCode ='"+dealerCode+"' AND month='"+month+"' AND year='"+year+"' AND asmId="+localStorage.getItem('USER_ID');
          }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
            $scope.zoStatus = true;
            var query = "SELECT * FROM dealers WHERE dealerCode ='"+dealerCode+"' AND month='"+month+"' AND year='"+year+"' AND zoId="+localStorage.getItem('USER_ID');
          }else{
            $scope.tsmStatus = true;
            var query = "SELECT * FROM dealers WHERE dealerCode ='"+dealerCode+"' AND month='"+month+"' AND year='"+year+"' AND tsmId="+localStorage.getItem('USER_ID');
          }
				//var query = "SELECT * FROM dealers WHERE dealerCode ='"+dealerCode+"' AND month='"+month+"' AND year='"+year+"'";
			}else{
          if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
            var query = "SELECT * FROM dealers WHERE month='"+month+"' AND year='"+year+"' AND tsmId="+localStorage.getItem('USER_ID');
            $scope.tsmStatus = true;
          }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
            $scope.asmStatus = true;
            var query = "SELECT * FROM dealers WHERE month='"+month+"' AND year='"+year+"' AND asmId="+localStorage.getItem('USER_ID');
          }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
            $scope.zoStatus = true;
            var query = "SELECT * FROM dealers WHERE month='"+month+"' AND year='"+year+"' AND zoId="+localStorage.getItem('USER_ID');
          }else{
            $scope.tsmStatus = true;
            var query = "SELECT * FROM dealers WHERE month='"+month+"' AND year='"+year+"' AND tsmId="+localStorage.getItem('USER_ID');
          }
				//var query = "SELECT * FROM dealers WHERE month='"+month+"' AND year='"+year+"'";
			}
			$cordovaSQLite.execute(db, query).then(function (result) {
				//console.log(result);

				for (var i = 0; i < result.rows.length; i++) {
					response.push(result.rows[i]);
				}
				//$scope.dealersData = response;
				$ionicLoading.hide();
			}, function (err) {
				console.error(err);
			});
		}

	}
	$scope.filterByMonth = function(data){
		//console.log(data)
		var response = [];
		$ionicLoading.show();

		localStorage.setItem("filterMonth", data);

		var dealerCode = localStorage.getItem("filterDealerCode");
		var month = localStorage.getItem("filterMonth");
		var year = localStorage.getItem("filterYear");
		//alert(dealerCode)
		//var searchByRefId = generateReferenceId(month, year, dealerCode, localStorage.getItem('empCode'));
		////console.log(searchByRefId)
    var stats = {};
    stats.loggedInEmpDesignation = localStorage.getItem('USER_DESIGNATION');
    stats.empId = localStorage.getItem('USER_ID');

				if(checkNetworkConnection()){
					$ionicLoading.show();

          if(dealerCode!=-1){
               if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsmId="+localStorage.getItem('USER_ID');
                  $scope.tsmStatus = true;
                }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                  $scope.asmStatus = true;
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND asmId="+localStorage.getItem('USER_ID');
                }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                  $scope.zoStatus = true;
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND zoId="+localStorage.getItem('USER_ID');
                }else{
                  $scope.tsmStatus = true;
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsmId="+localStorage.getItem('USER_ID');
                }
					}else{
              if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND tsmId="+localStorage.getItem('USER_ID');
                $scope.tsmStatus = true;
              }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                $scope.asmStatus = true;
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND asmId="+localStorage.getItem('USER_ID');
              }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                $scope.zoStatus = true;
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND zoId="+localStorage.getItem('USER_ID');
              }else{
                $scope.tsmStatus = true;
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND tsmId="+localStorage.getItem('USER_ID');
              }
             // var query = "SELECT * from dealers WHERE month="+month+" AND year="+year;
					}

						$cordovaSQLite.execute(db, query).then(function (result) {
							////console.log(result);
							var response = [];
							if(result.rows.length>0){

								for (var i = 0; i < result.rows.length; i++) {
									response.push(result.rows[i]);
								}
								$state.go('app.dashboard');
								$scope.dealersData = response;
								//$scope.dealersDataList = response;
								//console.log(response);
								$scope.month = result.rows[0].month;
								$scope.monthText = monthNames[month-1];
								$scope.year = result.rows[0].year;
								$ionicLoading.hide();
							}else{
								var url = BASE_URL + 'filterDealerList?userId=' + localStorage.getItem('USER_ID') + '&month=' + localStorage.getItem('filterMonth') + '&year=' + localStorage.getItem('filterYear') + '&dealerCode=' + dealerCode;
								////console.log(url)
										getDealerService.getDealers(url, "").then(function (dataIncoming) {
										////console.log(dataIncoming.jsonData)
										var data = dataIncoming.jsonData.dealerData;

										if (data != undefined || data != null) {
                      stats.empCode = dataIncoming.jsonData.stats.empCode;
											for (var i = 0; i < data.length; i++)
											{
												DEALERS.add(data[i],stats);
											}
                        $scope.month = data[0]['month'];
                        $scope.monthText = data[0]['monthText'];
                        $scope.year = data[0]['year'];
                        /*$state.go('app.dashboard');
                        $scope.dealersData = data;
                        //$scope.dealersDataList = response;
                        //console.log(response)
                        $ionicLoading.hide();*/

						if(dealerCode!=-1){
                            if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                               var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsmId="+localStorage.getItem('USER_ID');
                               $scope.tsmStatus = true;
                             }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                               $scope.asmStatus = true;
                               var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND asmId="+localStorage.getItem('USER_ID');
                             }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                               $scope.zoStatus = true;
                               var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND zoId="+localStorage.getItem('USER_ID');
                             }else{
                               $scope.tsmStatus = true;
                               var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsmId="+localStorage.getItem('USER_ID');
                             }
                      }else{
                           if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                             var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND tsmId="+localStorage.getItem('USER_ID');
                             $scope.tsmStatus = true;
                           }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                             $scope.asmStatus = true;
                             var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND asmId="+localStorage.getItem('USER_ID');
                           }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                             $scope.zoStatus = true;
                             var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND zoId="+localStorage.getItem('USER_ID');
                           }else{
                             $scope.tsmStatus = true;
                             var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND tsmId="+localStorage.getItem('USER_ID');
                           }
                          // var query = "SELECT * from dealers WHERE month="+month+" AND year="+year;
                      }
                      ////console.log(query)
                        $cordovaSQLite.execute(db, query).then(function (result) {
                          ////console.log(result);
                          for (var i = 0; i < result.rows.length; i++) {
                            response.push(result.rows[i]);
                          }

                          //console.log(response)
                           $scope.dealersData = response;
                          //$scope.dealersDataList = response;
                          $ionicLoading.hide();
                          $state.go('app.dashboard');
                        }, function (err) {
                          console.error(err);
                        });

										} else {
											//Close loader
											$scope.formLoader = false;
											$ionicLoading.hide();
										}
									}).catch(function (data) {
										alert(data)
										$ionicLoading.hide();
									});
							}
						}, function (err) {
							console.error(err);
						});
				}else{
          if(dealerCode!=-1){
               if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsmId="+localStorage.getItem('USER_ID');
                  $scope.tsmStatus = true;
                }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                  $scope.asmStatus = true;
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND asmId="+localStorage.getItem('USER_ID');
                }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                  $scope.zoStatus = true;
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND zoId="+localStorage.getItem('USER_ID');
                }else{
                  $scope.tsmStatus = true;
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsmId="+localStorage.getItem('USER_ID');
                }
          }else{
              if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND tsmId="+localStorage.getItem('USER_ID');
                $scope.tsmStatus = true;
              }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                $scope.asmStatus = true;
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND asmId="+localStorage.getItem('USER_ID');
              }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                $scope.zoStatus = true;
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND zoId="+localStorage.getItem('USER_ID');
              }else{
                $scope.tsmStatus = true;
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND tsmId="+localStorage.getItem('USER_ID');
              }
             // var query = "SELECT * from dealers WHERE month="+month+" AND year="+year;
          }
					$cordovaSQLite.execute(db, query).then(function (result) {
						//console.log(result);
						for (var i = 0; i < result.rows.length; i++) {
							response.push(result.rows[i]);
						}
						$scope.dealersData = response;
						//$scope.dealersDataList = response;
						$ionicLoading.hide();
					}, function (err) {
						console.error(err);
					});
				}

	}
	$scope.filterByYear = function(data){
		//console.log(data)
		var response = [];
		$ionicLoading.show();
		localStorage.setItem("filterYear", data);

		var dealerCode = localStorage.getItem("filterDealerCode");
		var month = localStorage.getItem("filterMonth");
		var year = localStorage.getItem("filterYear");
		//var searchByRefId = generateReferenceId(month, year, dealerCode, localStorage.getItem('empCode'));
    var stats = {};
    stats.loggedInEmpDesignation = localStorage.getItem('USER_DESIGNATION');
    stats.empId = localStorage.getItem('USER_ID');
		if(checkNetworkConnection()){
					$ionicLoading.show();
					//var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND referenceId="+searchByRefId;
					 if(dealerCode!=-1){
               if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsmId="+localStorage.getItem('USER_ID');
                  $scope.tsmStatus = true;
                }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                  $scope.asmStatus = true;
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND asmId="+localStorage.getItem('USER_ID');
                }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                  $scope.zoStatus = true;
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND zoId="+localStorage.getItem('USER_ID');
                }else{
                  $scope.tsmStatus = true;
                  var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsmId="+localStorage.getItem('USER_ID');
                }
          }else{
              if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsmId="+USER_ID;
                $scope.tsmStatus = true;
              }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                $scope.asmStatus = true;
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND asmId="+localStorage.getItem('USER_ID');
              }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                $scope.zoStatus = true;
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND zoId="+localStorage.getItem('USER_ID');
              }else{
                $scope.tsmStatus = true;
                var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND tsmId="+localStorage.getItem('USER_ID');
              }
             // var query = "SELECT * from dealers WHERE month="+month+" AND year="+year;
          }
						$cordovaSQLite.execute(db, query).then(function (result) {
							////console.log(result);
							var response = [];
							if(result.rows.length>0){

								for (var i = 0; i < result.rows.length; i++) {
									response.push(result.rows[i]);
								}
								$state.go('app.dashboard');
								$scope.dealersData = response;
								//$scope.dealersDataList = response;
								$scope.month = result.rows[0].month;
								$scope.monthText = monthNames[month-1];
								$scope.year = result.rows[0].year;
								$ionicLoading.hide();
							}else{
								var url = BASE_URL + 'filterDealerList?userId=' + localStorage.getItem('USER_ID') + '&month=' + localStorage.getItem('filterMonth') + '&year=' + localStorage.getItem('filterYear') + '&dealerCode=' + dealerCode;
									getDealerService.getDealers(url, "").then(function (dataIncoming) {
										////console.log(dataIncoming.jsonData)
										var data = dataIncoming.jsonData.dealerData;

										if (data != undefined || data != null) {
                      stats.empCode = dataIncoming.jsonData.stats.empCode;
											for (var i = 0; i < data.length; i++)
											{
												//console.log("add dealer");
												DEALERS.add(data[i],stats);
											}
												$scope.month = data[0]['month'];
												$scope.monthText = data[0]['monthText'];
												$scope.year = data[0]['year'];
												$state.go('app.dashboard');
												$scope.dealersData = data;
												//$scope.dealersDataList = response;
												$ionicLoading.hide();
										} else {
											//Close loader
											$scope.formLoader = false;
											$ionicLoading.hide();
										}
									}).catch(function (data) {
										alert(data)
										$ionicLoading.hide();
									});
							}
						}, function (err) {
							console.error(err);
						});
				}else{
						 if(dealerCode!=-1){
                           if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                              var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsm="+localStorage.getItem('USER_ID');
                              $scope.tsmStatus = true;
                            }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                              $scope.asmStatus = true;
                              var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND asmId="+localStorage.getItem('USER_ID');
                            }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                              $scope.zoStatus = true;
                              var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND zoId="+localStorage.getItem('USER_ID');
                            }else{
                              $scope.tsmStatus = true;
                              var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsmId="+localStorage.getItem('USER_ID');
                            }
                      }else{
                          if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
                            var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND dealerCode="+dealerCode+" AND tsm="+localStorage.getItem('USER_ID');
                            $scope.tsmStatus = true;
                          }else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
                            $scope.asmStatus = true;
                            var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND asmId="+localStorage.getItem('USER_ID');
                          }else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
                            $scope.zoStatus = true;
                            var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND zoId="+localStorage.getItem('USER_ID');
                          }else{
                            $scope.tsmStatus = true;
                            var query = "SELECT * from dealers WHERE month="+month+" AND year="+year+" AND tsmId="+localStorage.getItem('USER_ID');
                          }
                         // var query = "SELECT * from dealers WHERE month="+month+" AND year="+year;
                      }
						$cordovaSQLite.execute(db, query).then(function (result) {
							//console.log(result);
							for (var i = 0; i < result.rows.length; i++) {
								response.push(result.rows[i]);
							}
							$scope.dealersData = response;
							//$scope.dealersDataList = response;
							$ionicLoading.hide();
						}, function (err) {
							console.error(err);
						});
				}
	}
})

/**
 @Name: guideLineCtrl
 @Type: Controller
 @Author: Anjani Kr Gupta
 @Date: 4-Jul-2016
 */
.controller('guideLineCtrl', function ($ionicHistory, $scope, $http, $state, $ionicLoading, survey, guideLine, $ionicHistory) {
	//$ionicHistory.clearHistory();
	//$ionicHistory.clearCache();
if(localStorage.getItem('USER_ID')=="" || localStorage.getItem('USER_ID')==null){
	$state.go('login');
}else{
	var currentdate = new Date();

	$scope.currentDateTime = currentdate.getDate() + "-"
                + (currentdate.getMonth()+1)  + "-"
                + currentdate.getFullYear() + "  "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();
	//alert(datetime);
	$scope.visitDate = currentdate.getDate() + "-"
					+ (currentdate.getMonth()+1)  + "-"
					+ currentdate.getFullYear();
	$scope.updateVisitDate = function (val) {
			var d = new Date(val);
			$scope.visitDate = d.getDate() + "-"
					+ (d.getMonth()+1)  + "-"
					+ d.getFullYear();

			var upData = {referenceId: localStorage.getItem('referenceId'), surveyId: localStorage.getItem('surveyId'), visitDate: $scope.visitDate};
			//console.log(upData);
			survey.updateSurveyBasicInfoVisitDate(upData).then(function(res){});
		}
	$scope.menu = false;
	$scope.survey_status = localStorage.getItem('survey_status');
	$scope.btnTxt = localStorage.getItem('btnTxt');
	$scope.btnStatus = localStorage.getItem('btnStatus');

	if(checkNetworkConnection()){

		var url = BASE_URL + 'guidelines?tsmId='+ localStorage.getItem('USER_ID') +'&dealerCode=' + localStorage.getItem("dealerCode");
		console.log(url)
		guideLine.getGuideLine(url, "").then(function(data){
			if(data.status){
				var upData = {guideLine: data.jsonData[0].guideLines  , referenceId: localStorage.getItem('referenceId'), surveyId: localStorage.getItem('surveyId')};
				////console.log(upData)
				survey.updateSurveyBasicInfo(upData).then(function(res){
					survey.getGuideLineData(upData).then(function(result){
						console.log(result);
						var r = result.rows[0];
						$scope.dealerName = r.dealerName;
						$scope.dealerCode = r.dealerCode;
						$scope.dealerMappedStaffDepartment =  r.empDepartment;
						$scope.empName = r.empName;

						$scope.dateValue= r.survey_visited_date;
						var g = r.guideline;
						$scope.guidLineContent = g.split(",");
					})
				})
			}
		}).catch(function (data) {
			alert(data)
			$ionicLoading.hide();
		});
	}else{
		var data = {referenceId: localStorage.getItem('referenceId'), surveyId: localStorage.getItem('surveyId')};
		survey.getGuideLineData(data).then(function(result){
			////console.log(result);
			if(result.rows.length>0){
					var r = result.rows[0];
					$scope.dealerName = r.dealerName;
					$scope.dealerCode = r.dealerCode;
					$scope.dealerMappedStaffDepartment =  r.empDepartment;
					$scope.empName = r.empName;
					$scope.dateValue= r.survey_visited_date;
					var g = r.guideline;
					//console.log(g)
					if(g!="" && g!=null){
							$scope.guidLineContent = g.split(",");
					}else{
						$scope.guidLineContent = "";
					}

			}else{
				alert("Sorry, no Internet connectivity detected. Please reconnect and try again.");
				$state.go('app.dashboard');
			}
		});
	}
}
})
/**
 @Name: salesDataCtrl
 @Type: Controller
 @Author: Anjani Kr Gupta
 @Date: 10-Jul-2016
 */
.controller('salesDataCtrl', function ($scope, $http, $state, $ionicLoading, survey, $rootScope, $cordovaSQLite, getSalesDataService) {
if(localStorage.getItem('USER_ID')=="" || localStorage.getItem('USER_ID')==null){
	$state.go('login');
}else{
	$scope.menu = false;
	$scope.dealerCode = localStorage.getItem("dealerCode");
	if(checkNetworkConnection()){
		var url = BASE_URL + 'salesData?dealerCode=' + localStorage.getItem('dealerCode') + '&month=' + localStorage.getItem('month') + '&year=' + localStorage.getItem('year');
		$ionicLoading.show();
		getSalesDataService.getSalesData(url,"").then(function (response) {
			if(response.status){
				var data = response.jsonData;
				////console.log(data)
				survey.addSalesData(data).then(function (res)
				{
					survey.getSurveySalesData().then(function (result) {
						$ionicLoading.hide();
						var salesArray = [];
						var servicesArray = [];
						for (var i = 0; i < result.length; i++) {
							if (result[i].rows[0].sales_data_type == "sales") {
								salesArray.push(result[i]);
							} else {
								servicesArray.push(result[i]);
							}
						}
						$scope.salesData = salesArray;
						$scope.servicesData = servicesArray;
					});
				}).catch(function (error) {
					$ionicLoading.hide();
				});
			}else{
				alert(response.message);
			}
		}).catch(function (data) {
			alert(data)
			$ionicLoading.hide();
		});
	}else{
		survey.getSurveySalesData().then(function (result) {
				if(result.length>0){
					$ionicLoading.hide();
					var salesArray = [];
					var servicesArray = [];
					for (var i = 0; i < result.length; i++) {
						if (result[i].rows[0].sales_data_type == "sales") {
							salesArray.push(result[i]);
						} else {
							servicesArray.push(result[i]);
						}
					}
					$scope.salesData = salesArray;
					$scope.servicesData = servicesArray;
				}else{
					alert("Sorry, no Internet connectivity detected. Please reconnect and try again.");
					$state.go('app.dashboard');
				}
		}).catch(function (error) {
			$ionicLoading.hide();
		});
	}
}
})
/**
 @Name: salesNormsCtrl
 @Type: Controller
 @Author: Anjani Kr Gupta
 @Date: 15-Jul-2016
 */
.controller('salesNormsCtrl', function ($scope, $http, $state, $ionicLoading, survey, $rootScope, getNormsService, createSurvey, $ionicSlideBoxDelegate) {
if(localStorage.getItem('USER_ID')=="" || localStorage.getItem('USER_ID')==null){
	$state.go('login');
}else{
$scope.isNumeric = function(val){
	 var pattern = /^\+?[0-9]*\.?[0-9]+$/;
     var res = pattern.test(val);
	 //alert(res)
	 if(!isNaN(val) && res==true){
		 return true;
	 }else{
		 return false;
	 }
}
$scope.checked = {};
$scope.totalSlide = [0,1];

$scope.currentIndex = 1;
$scope.slideIndex = 1;
$scope.slideChanged = function(index) {
    $scope.slideIndex = index;
	//alert(index)
  };
/* $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  }; */
$scope.init = function () {

};
$scope.survey_status = localStorage.getItem('survey_status');
$scope.btnTxt = localStorage.getItem('btnTxt');
$scope.btnStatus = localStorage.getItem('btnStatus');
	$scope.menu = false;
	$scope.dealerCode = localStorage.getItem('dealerCode');
	var resData = [];
	if(checkNetworkConnection()){
		var url = BASE_URL + 'norms?tsmId=' + localStorage.getItem('USER_ID') + '&dealerCode=' + localStorage.getItem('dealerCode');
		console.log(url)
		$ionicLoading.show();
		getNormsService.getNorms(url, "").then(function (response) {
			//console.log(response)
			if(response.status){
				////console.log(response.jsonData[])
				if (response.jsonData.hasOwnProperty('Sales_norms')) {
					var data = response.jsonData.Sales_norms;
					var nKey = 'sales';
				}else{
					var data = response.jsonData.Services_norms;
					var nKey = 'services';
				}
				survey.addNorms(data,nKey).then(function (res)
				{
					var norms = salesNorms(data,nKey);
					createSurvey.submitSalesNorms(norms).then(function(){
						survey.getSurveyNorms( localStorage.getItem('surveyId'), localStorage.getItem('referenceId')).then(function (result) {
							console.log(result)
							$ionicLoading.hide();
							for (var i = 0; i < result.rows.length; i++) {

								resData.push(result.rows[i]);

								if(result.rows[i].require_value==""){
									 $scope.checked[i] = false;

								}
								if(result.rows[i].actual_value==""){
									 $scope.checked[i] = false;

								}
							}
							//console.log(resData)
							$scope.normsPlan = resData;
							$scope.getNormsData(result.rows[0].alias,'0');
						});
					});
				}).catch(function (error) {
					$ionicLoading.hide();
				});
			}else{
				alert(response.message);
			}
		}).catch(function (data) {
			alert(data)
			$ionicLoading.hide();
		});
	}else{
		$ionicLoading.show();
		survey.getSurveyNorms(localStorage.getItem('surveyId'),localStorage.getItem('referenceId')).then(function (result) {
			//console.log(result)
			if(result.rows.length>0){
				$ionicLoading.hide();
				for (var i = 0; i < result.rows.length; i++) {
					resData.push(result.rows[i]);
				}
				$scope.normsPlan = resData;
				$scope.getNormsData(result.rows[0].alias,'0');
			}else{
				$ionicLoading.hide();
				alert("Sorry, no Internet connectivity detected. Please reconnect and try again.");
				$state.go('app.dashboard');
			}
		});
	}
}
	$scope.alias = "";
	$scope.index = 0;

	$scope.requireValueType = "";
	$scope.hideContent = false;

	$scope.availableNos = false;
	$scope.requiredNos = false;

	$scope.getNormsData = function (alias,index) {
		//alert(index)
		$scope.index = index;
		$scope.alias = alias;
		$scope.availableValue ="";
		$scope.formData = {};
		//$scope.availableNos = false;
		//alert($rootScope.dateValue)
		//$scope.dateValue = "yyyy-mm-dd";
		//alert($rootScope.dateValue)
		////console.log($scope.dateValue)
		if(alias!=null){
			$scope.hideContent = true;
			$ionicLoading.show();
			survey.getSurveyNorm(alias).then(function(res){
				console.log(res)
				var d = res.rows[0];
				$scope.formData.requireValueType = d.requiredValueType;
				$scope.dealerCode = localStorage.getItem('dealerCode');
				$scope.designation = d.designation;
				$scope.formData.requiredValue = d.requiredValue;
				$scope.formData.availableValue = d.actualValue;
				$scope.formData.action_plan = d.actionPlan;
				$scope.formData.action_plan_status = d.actionPlanStatus;
				$scope.formData.action_plan_asm = d.actionPlanAsm;
				$scope.formData.action_plan_asm_status = d.actionPlanAsmStatus;
				$scope.formData.action_plan_zm = d.actionPlanZm;
				$scope.formData.action_plan_zm_status = d.actionPlanZmStatus;
				$scope.formData.target_date = d.targetDate;
				if(d.targetDate!=""){
					$scope.dateValue = d.targetDate;
				}else{
					$scope.dateValue = "dd-mm-yyyy";
				}
				 var pattern = /^\+?[0-9]*\.?[0-9]+$/;
				 var res = pattern.test(d.actualValue);
				 var resR = pattern.test(d.requiredValue);
				 if(!res && d.actualValue!=''){
					 $scope.availableNos = true;
				 }else{
					 $scope.availableNos = false;
				 }
				 if(!resR && d.requiredValue!='As per need' && d.requiredValue!=''){
					 $scope.requiredNos = true;
				 }else{
					 $scope.requiredNos = false;
				 }
				 $ionicSlideBoxDelegate.next();
				 $ionicLoading.hide();
			});
		}else{
			$scope.hideContent = false;
		}

	}

	$scope.updateRequiredValue = function (val) {
		 var pattern = /^\+?[0-9]*\.?[0-9]+$/;
		 var res = pattern.test(val);
		 if(!res){
			 $scope.requiredNos = true;
			 $scope.checked[$scope.index] = false;
		 }else{
			 $scope.requiredNos = false;
			 $scope.checked[$scope.index] = true;
		 }
		var data = {};
		data.column = 'require_value';
		data.alias = $scope.alias;
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		data.fieldVal = val;
		createSurvey.updateNormsData(data).then(function(result){
			//console.log(result);
		});
	}

	$scope.updateAvailableValue = function (val) {
		//alert($scope.index)
		//alert($scope.alias)
		 var pattern = /^\+?[0-9]*\.?[0-9]+$/;
		 var res = pattern.test(val);
		 if(!res){
			 $scope.availableNos = true;
			 $scope.checked[$scope.index] = false;
		 }else{
			 $scope.availableNos = false;
			 $scope.checked[$scope.index] = true;
			 //alert(str)
		 }
		var data = {};
		data.column = 'actual_value';
		data.alias = $scope.alias;
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		data.fieldVal = val;
		createSurvey.updateNormsData(data).then(function(result){
			//console.log(result);
		});
	}
	$scope.updateTargetDate = function (val) {
		var d = new Date(val);
		$scope.targetDate = d.getDate() + "-" + (d.getMonth()+1)  + "-" + d.getFullYear();
		var data = {};
		data.column = 'target_date';
		data.alias = $scope.alias;
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		data.fieldVal = $scope.targetDate;
		createSurvey.updateNormsData(data).then(function(result){
			//console.log(result);
		});
	}
	$scope.updatePlanTSM = function (val) {
		var data = {};
		data.column = 'action_plan';
		data.alias = $scope.alias;
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		data.fieldVal = val;
		createSurvey.updateNormsData(data).then(function(result){
			////console.log(result);
		});
	}
	$scope.updatePlanASM = function (val) {
		var data = {};
		data.column = 'action_plan_asm';
		data.alias = $scope.alias;
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		data.fieldVal = val;
		createSurvey.updateNormsData(data).then(function(result){
			//console.log(result);
		});
	}
	$scope.updatePlanZM = function (val) {
		var data = {};
		data.column = 'action_plan_zm';
		data.alias = $scope.alias;
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		data.fieldVal = val;
		createSurvey.updateNormsData(data).then(function(result){
			//console.log(result);
		});
	}
})
/**
 @Name: surveyQuestions
 @Type: Controller
 @Author: Anjani Kr Gupta
 @Date: 13th-Aug-2016
 */
.controller('surveyQuestions', function ($scope, $http, $state, $ionicLoading, $cordovaSQLite, getQuestionsService, $rootScope, survey, createSurvey, Camera, $cordovaFileTransfer, $ionicSlideBoxDelegate ) {
$scope.currentIndex = 1;
$scope.survey_status = localStorage.getItem('survey_status');
$scope.btnTxt = localStorage.getItem('btnTxt');
$scope.btnStatus = localStorage.getItem('btnStatus');

$scope.totalSlide = [0,1];

$scope.slideIndex = 1;
/* $scope.slideChanged = function(index) {
    $scope.slideIndex = index;
	//alert(index)
  }; */

$scope.targetDateErr = {};
$scope.isDateSet = {};
$scope.actionPlanTSMErr = {};
$scope.actionPlanASMErr = {};
$scope.actionPlanZOErr = {};
$scope.isValidated = {};

$scope.slideChanged = function(index){
	 $scope.slideIndex = index;
	if(index==0){
		//alert(localStorage.getItem('criteriaId'))
		var data = {};
		data.criteriaId = localStorage.getItem('criteriaId');
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		createSurvey.getSurveyQuestionsData(data).then(function(result){
			var c = 0;
			var n = 0;
			if(result.rows.length>0){
				for(var i=0;i<result.rows.length;i++){
					n=n+1;
					if(result.rows[i].isValidated==0){
						//alert('f')
						//$scope.isValidated[data.criteriaId] = true;
						c=c+1;
					}
					if(n===result.rows.length){
						//alert(n)
						if(c==0){
							//update survey criteria status
							data.status = 1;
							createSurvey.updateSurveyCriteriaStatus(data).then(function(result){

										survey.getCriteria().then(function(res){
											//console.log(res)
											if(res.rows.length>0){
												//$scope.criteria = res.rows;
													var crArray = [];
													for (var i = 0; i < res.rows.length; i++) {
														crArray.push(res.rows[i]);
													}
													$scope.criteria = crArray;
											}
										});

							});
						}else{
							//alert(true)
							//update survey criteria status
							data.status = 0;
							createSurvey.updateSurveyCriteriaStatus(data).then(function(result){

										survey.getCriteria().then(function(res){
											//console.log(res)
											if(res.rows.length>0){
												//$scope.criteria = res.rows;
													var crArray = [];
													for (var i = 0; i < res.rows.length; i++) {
														crArray.push(res.rows[i]);
													}
													$scope.criteria = crArray;
											}
										});

							});
						}
					}

				}

			}
		});
	}
}
if(localStorage.getItem('USER_ID')=="" || localStorage.getItem('USER_ID')==null){
	$state.go('login');
}else{
$scope.menu = false;
	var response = [];
	var resData = [];

	$scope.quesContainer = false;
	$scope.dealerCode = localStorage.getItem('dealerCode');
	if(checkNetworkConnection()){
			var url = BASE_URL + 'criteria?tsmId=' + localStorage.getItem('USER_ID') + '&dealerCode=' + localStorage.getItem('dealerCode');
			console.log(url);
			$ionicLoading.show();

			getQuestionsService.getQuestions(url, "").then(function (data) {
				if(data.status){
					//console.log(data.jsonData)
					var res = data.jsonData;
					var q = [];
					var c = [];
					var ddData = [];

					for(key in res){
						for(ques in res[key]['questions']){
							//console.log(res[key]['questions'][ques])
							var questions = {};
							var d = {};
							questions.criteriaId = key;
							questions.criteriaName = res[key]['criteria_name'][0];
							questions.questionId = ques;
							questions.questionName = res[key]['questions'][ques]['question_name']['#markup'];
							questions.actionPlan = res[key]['questions'][ques]['action_plan']['#prefixData']['content'];
							questions.actionPlanAsm = res[key]['questions'][ques]['action_plan_asm']['#prefixData']['content'];
							questions.actionPlanZm = res[key]['questions'][ques]['action_plan_zm']['#prefixData']['content'];
							questions.actionPlanStatus = res[key]['questions'][ques]['action_plan']['#disabled'];
							questions.actionPlanAsmStatus = res[key]['questions'][ques]['action_plan_asm']['#disabled'];
							questions.actionPlanZmStatus = res[key]['questions'][ques]['action_plan_zm']['#disabled'];
							d.isValidated = 0;
							d.criteriaId = key;
							d.criteriaName = res[key]['criteria_name'][0];
							ddData.push(d);
							q.push(questions);
							c.push(key);

						}
					}
					//console.log(c[0])
					survey.addCriteriaQuetions(q,c).then(function(result){

							var seen = {};
							var uniqueCr = ddData.filter(function(item){
								if(seen.hasOwnProperty(item.criteriaId)){
									return false;
								}else{
									seen[item.criteriaId] = true;
									return true;
								}
							});
							//$ionicLoading.hide();
							////console.log(uniqueCr)
							//$scope.criteria = uniqueCr;
							//$scope.getCriteriaQues('232','0');
							survey.getCriteria().then(function(res){
								console.log(res)
								if(res.rows.length>0){
									//$scope.criteria = res.rows;
										var crArray = [];
										for (var i = 0; i < res.rows.length; i++) {
											crArray.push(res.rows[i]);
										}
										$scope.criteria = crArray;
										$scope.getCriteriaQues(res.rows[0].criteriaId,'0');
										$ionicLoading.hide();
								}else{
									console.log(c)
									$ionicLoading.hide();
									$scope.criteria = uniqueCr;
									$scope.getCriteriaQues(c[0],'0');
								}
							});
					}).catch(function (error) {
						$ionicLoading.hide();
					});
				}else{
					$ionicLoading.hide();
					alert(data.message);
				}
			}).catch(function (data) {
				alert(data)
				$ionicLoading.hide();
			});
	}else{
		survey.getCriteria().then(function(res){
			//console.log(res)
			if(res.rows.length>0){
				//$scope.criteria = res.rows;
					var crArray = [];
					for (var i = 0; i < res.rows.length; i++) {
						crArray.push(res.rows[i]);
					}
					$scope.criteria = crArray;
					$scope.getCriteriaQues('232','0');
			}else{
				alert("Sorry, no Internet connectivity detected. Please reconnect and try again.");
				$state.go('app.dashboard');
			}
		});
	}
}

	$scope.getCriteriaQues = function(crId,index){
	$scope.cID = crId;
		$ionicLoading.show();
		localStorage.setItem("criteriaId",crId);
		$scope.quesContainer = true;
		survey.getCriteriaQuestions(crId).then(function(res){
				console.log(res);
				var quesData = [];
				for (var i = 0; i < res.rows.length; i++) {
					quesData.push(res.rows[i]);
				}
				$ionicLoading.hide();
				$scope.quesData = quesData;
				$ionicSlideBoxDelegate.next();
				console.log(quesData);
			}).catch(function (error) {
				$ionicLoading.hide();
				$scope.quesContainer = false;
			});
	}
	$scope.getResponse = function(simple,x){
		alert(x)
	}
	//update response
	$scope.updateResponse = function (val,qId,index) {
		$scope.index = index;
		//$scope.checked[$scope.index] = false;

		if(!val){
			//alert(val)
			$scope.targetDateErr[$scope.index] = true;

			//$scope.isDateSet[index] = false;
			////console.log($scope.targetDateErr);
			if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
				//alert(index)
				$scope.actionPlanTSMErr[$scope.index] = true;
				$scope.actionPlanASMErr[$scope.index] = false;
				$scope.actionPlanZOErr[$scope.index] = false;
			}else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
				$scope.actionPlanTSMErr[$scope.index] = false;
				$scope.actionPlanASMErr[$$scope.index] = true;
				$scope.actionPlanZOErr[$scope.index] = false;
			}else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
				$scope.actionPlanTSMErr[$scope.index] = false;
				$scope.actionPlanASMErr[$scope.index] = false;
				$scope.actionPlanZOErr[$scope.index] = true;
			}
			var data = {};
			data.column = 'response';
			data.criteriaId = localStorage.getItem('criteriaId');
			data.questionId = qId;
			data.surveyId = localStorage.getItem('surveyId');
			data.referenceId = localStorage.getItem('referenceId');
			data.fieldVal = val;

			createSurvey.getSurveyData(data).then(function(result){

				//console.log(result.rows);

				if(result.rows.length>0){
					if(result.rows[0].actionPlanDate!="" && result.rows[0].actionPlan!=""){
						data.isValidated = 1;
						createSurvey.updateSurveyData(data).then(function(result){});
					}else{
						data.isValidated = 0;
						createSurvey.updateSurveyData(data).then(function(result){});
					}
				}

			});

		}else{
			$scope.targetDateErr[$scope.index] = false;
			//$scope.isDateSet[index] = true;
			$scope.actionPlanTSMErr[$scope.index] = false;
			$scope.actionPlanASMErr[$scope.index] = false;
			$scope.actionPlanZOErr[$scope.index] = false;
			//update
			var data = {};
			data.column = 'response';
			data.criteriaId = localStorage.getItem('criteriaId');
			data.questionId = qId;
			data.surveyId = localStorage.getItem('surveyId');
			data.referenceId = localStorage.getItem('referenceId');
			data.fieldVal = val;
			data.isValidated = 1;

			createSurvey.updateSurveyData(data).then(function(result){});
		}

		////console.log($scope.targetDateErr);
	}
	//update target date
	$scope.updateTargetDate = function (val,qId,index) {
		//alert(index)
		$scope.targetDateErr[index] = false;
		$scope.isDateSet[index] = true;
		var d = new Date(val);
		$scope.targetDate = d.getDate() + "-" + (d.getMonth()+1)  + "-" + d.getFullYear();

		var data = {};
		data.column = 'actionPlanDate';
		data.criteriaId = localStorage.getItem('criteriaId');
		data.questionId = qId;
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		data.fieldVal = $scope.targetDate;
		createSurvey.updateSurveyData(data).then(function(result){
			//console.log(result);
		});
	}
	//update tsm plan
	$scope.updatePlanTSM = function (val,qId,index) {
		if(val==""){
			//$scope.targetDateErr = true;
			if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
				$scope.actionPlanTSMErr[index] = true;
				$scope.actionPlanASMErr[index] = false;
				$scope.actionPlanZOErr[index] = false;
			}else if(localStorage.getItem('USER_DESIGNATION')=='asm'){
				$scope.actionPlanTSMErr[index] = false;
				$scope.actionPlanASMErr[index] = true;
				$scope.actionPlanZOErr[index] = false;
			}else if(localStorage.getItem('USER_DESIGNATION')=='zo'){
				$scope.actionPlanTSMErr[index] = false;
				$scope.actionPlanASMErr[index] = false;
				$scope.actionPlanZOErr = true;
			}
			var data = {};
			data.column = 'actionPlan';
			data.criteriaId = localStorage.getItem('criteriaId');
			data.questionId = qId;
			data.surveyId = localStorage.getItem('surveyId');
			data.referenceId = localStorage.getItem('referenceId');
			data.fieldVal = val;
			data.isValidated = 0;
			createSurvey.updateSurveyData(data).then(function(result){});

		}else{
			//$scope.targetDateErr = false;
			$scope.actionPlanTSMErr[index] = false;
			$scope.actionPlanASMErr[index] = false;
			$scope.actionPlanZOErr[index] = false;
			var data = {};
			data.column = 'actionPlan';
			data.criteriaId = localStorage.getItem('criteriaId');
			data.questionId = qId;
			data.surveyId = localStorage.getItem('surveyId');
			data.referenceId = localStorage.getItem('referenceId');
			data.fieldVal = val;
			createSurvey.getSurveyData(data).then(function(result){

				////console.log(result.rows);

				if(result.rows.length>0){
					if(result.rows[0].actionPlanDate!="" && result.rows[0].response!="true"){
						data.isValidated = 1;
						createSurvey.updateSurveyData(data).then(function(result){});
					}else{
						data.isValidated = 0;
						createSurvey.updateSurveyData(data).then(function(result){});
					}
				}

			});
		}

	}
	//update ASM plan
	$scope.updatePlanASM = function (val,qId,index) {
		var data = {};
		data.column = 'actionPlanAsm';
		data.criteriaId = localStorage.getItem('criteriaId');
		data.questionId = qId;
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		data.fieldVal = val;
		createSurvey.updateSurveyData(data).then(function(result){
			//console.log(result);
		});
	}
	//update ZM plan
	$scope.updatePlanZM = function (val,qId,index) {
		var data = {};
		data.column = 'actionPlanZm';
		data.criteriaId = localStorage.getItem('criteriaId');
		data.questionId = qId;
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		data.fieldVal = val;
		createSurvey.updateSurveyData(data).then(function(result){
			//console.log(result);
		});
	}
	var imgObj = {};
	var opts = {};
	 $scope.takePicture = function (criteriaId,flag) {
	 //alert(flag)
    var sourceType  = 1;
     // alert(criteriaId)
     if(flag==='camera'){
        sourceType = 1;
     }else if(flag==='gallery'){
        sourceType = 0;
     }
		  var options = {
			 quality : 75,
			 targetWidth: 200,
			 targetHeight: 200,
			 sourceType: sourceType,
			 popoverOptions: CameraPopoverOptions,
       saveToPhotoAlbum: true
		  };

		  Camera.getPicture(options).then(function(imageData) {
		        var img = {};
        //alert(JSON.stringify(imageData))
			      $scope.picture = imageData;

            var url = BASE_URL + 'upload';
            //target path may be local or url
            var targetPath = imageData;
            //alert(JSON.stringify(imageData));
            var filename = targetPath.split("/").pop();
            var options = {
                fileKey: "import_"+criteriaId,
                fileName: filename,
                chunkedMode: false,
                mimeType: "image/jpg"
            };
            //imgObj[criteriaId] = targetPath;
            //opts[criteriaId] = options;
            img.criteriaId = criteriaId;
            img.surveyId = localStorage.getItem('surveyId');
            img.referenceId = localStorage.getItem('referenceId');
            img.file = imageData;
            img.fileName = filename;
            createSurvey.uploadImg(img).then(function(res){
             // alert(JSON.stringify(res))
            });
/*            $cordovaFileTransfer.upload(url, imgObj, opts).then(function(result) {
                //console.log("SUCCESS: " + JSON.stringify(result));
                alert("success");
                alert(JSON.stringify(result));
            }, function(err) {
                //console.log("ERROR: " + JSON.stringify(err));
                alert(JSON.stringify(err));
            }, function (progress) {
                // constant progress updates
                $timeout(function () {
                    $scope.downloadProgress = (progress.loaded / progress.total) * 100;
                })
            });*/

		  }, function(err) {
			 //console.log(err);
		  });

	   }
	   $scope.getImage = function(){
        createSurvey.getAttachment().then(function(res){
              alert(JSON.stringify(res));
	   	  });
	   }
$scope.onSwipeLeft = function(){
	alert(1)
}
})
/**
 @Name: remark
 @Type: Controller
 @Author: Anjani Kr Gupta
 @Date: 13th-Aug-2016
 */
.controller('remark', function ($ionicHistory, $q, $scope, $http, $state, $ionicLoading, remarks, $rootScope, survey, createSurvey, submitSurveyService, $cordovaFileTransfer) {
if(localStorage.getItem('USER_ID')=="" || localStorage.getItem('USER_ID')==null){
	$state.go('login');
}else{

	$scope.isNumeric = function(val){
		 var pattern = /^\+?[0-9]*\.?[0-9]+$/;
		 var res = pattern.test(val);
		 //alert(res)
		 if(!isNaN(val) && res==true){
			 return true;
		 }else{
			 return false;
		 }
	}

	$scope.survey_status = localStorage.getItem('survey_status');
	$scope.btnTxt = localStorage.getItem('btnTxt');
  $scope.btnStatus = localStorage.getItem('btnStatus');
	$scope.menu = false;
	if(checkNetworkConnection()){
		var url = BASE_URL + 'remarks?tsmId=' + localStorage.getItem('USER_ID');
		$ionicLoading.show();
		$scope.dealerCode = localStorage.getItem('dealerCode');
		remarks.getRemarks(url, "").then(function (result) {
			////console.log(result)
			if(result.status){
				var data = result.jsonData;
				survey.addRemark(data).then(function(res){
					survey.getRemarks().then(function(r){
						////console.log(r.rows);
							var remark = [];
							for (var i = 0; i < r.rows.length; i++) {
								remark.push(r.rows[i]);
							}
							$scope.remark = remark;
					});
				}).catch(function(err){
					alert(err)
				});
			}else{
				alert(result.message)
			}
			$ionicLoading.hide();
		}).catch(function (data) {
			alert(data)
			$ionicLoading.hide();
		});
	}else{
		$scope.dealerCode = localStorage.getItem('dealerCode');
		////console.log(localStorage.getItem('referenceId'))
		survey.getRemarks().then(function(r){
			////console.log(r.rows);
			if(r.rows.length>0){
				var remark = [];
				for (var i = 0; i < r.rows.length; i++) {
					remark.push(r.rows[i]);
				}
				$scope.remark = remark;
			}else{
				alert("Sorry, no Internet connectivity detected. Please reconnect and try again.");
				$state.go('app.dashboard');
			}
		});
	}

	//update remark
	$scope.updateRemark = function (val,title) {
		var data = {};
		data.title = title;
		data.surveyId = localStorage.getItem('surveyId');
		data.referenceId = localStorage.getItem('referenceId');
		data.fieldVal = val;
		createSurvey.updateRemark(data).then(function(result){
			//console.log(result);
		});
	}
	//submit survey
	$scope.submitSurvey = function(){
		if(checkNetworkConnection()){
			$ionicLoading.show();
			var survey_information = {};
			var alias = {};
			var norms = {};
			var normArray = {};
			var criterias = {};
			var question = {};
			var sid = "";
			//get survey information
			createSurvey.getSurveyInformation().then(function(res){
					////console.log(res.rows[0]);
					if(res.rows.length>0){
						sid = res.rows[0].serverSid;
						survey_information.survey_name = res.rows[0].survey_name;
						survey_information.survey_status_id = 2; //submitted
						survey_information.asso_dealer_code = res.rows[0].asso_dealer_code;
						survey_information.survey_for_dept_code = res.rows[0].survey_for_dept_code;
						survey_information.survey_owner_emp_code = res.rows[0].survey_owner_emp_code;
						survey_information.survey_created_date = res.rows[0].survey_created_date;
						survey_information.visited_date_time = res.rows[0].survey_visited_date;
						survey_information.survey_modified_date = res.rows[0].survey_modified_date;
						survey_information.survey_month = res.rows[0].survey_month;
						survey_information.survey_year = res.rows[0].survey_year;
						survey_information.remark = res.rows[0].survey_remark;
						survey_information.flag = res.rows[0].flag;
						////console.log(survey_information);
					}
					if(res.rows[0].survey_for_dept_code=="1"){
						var dep = 'Sales_norms';
					}else if(res.rows[0].survey_for_dept_code=="2"){
						var dep = 'Services_norms';
					}else{
						var dep = undefined;
					}
					//get survey norms
					createSurvey.getNorms().then(function(res){
						////console.log(res.rows);
						if(res.rows.length>0){
							var data = res.rows;
							var n=0;
							for(var i=0;i<data.length;i++){
								////console.log(data[i]);
								var aliasData = {};
								var normsData = {};
								aliasData.designation = data[i].designation;
								aliasData.require_value = data[i].require_value;
								aliasData.actual_value = data[i].actual_value;
								var key = data[i].alias;
								norms[key] = aliasData;

								normsData.designation = data[i].designation;
								normsData.actual_status = data[i].actual_value;
								normsData.required_value = data[i].require_value;
								normsData.action_plan = data[i].action_plan;
								normsData.action_plan_asm = data[i].action_plan_asm;
								normsData.action_plan_zm = data[i].action_plan_zm;
								normsData.target_date = data[i].target_date;
								var keyNorms = data[i].alias;
								normArray[keyNorms] = normsData;

									if(data[i].actual_value=="" || data[i].require_value=="" || !$scope.isNumeric(data[i].actual_value)){
										n = n+1;
									}

							}
							//JSON.stringify(norms);
							//JSON.stringify(normArray);
							////console.log(normArray)
						}
						 //get survey questions
						 createSurvey.getCriteria().then(function(res){
							 //console.log(res)
							 var cr = 0;
							 if(res.length>0){
								 for(var i=0;i<res.length;i++){
									 var ques = {};
									 if(res[i].rows.length>0){
										 for(var j=0;j<res[i].rows.length;j++){
												var q = {};
												q.criteria_id = res[i].rows[j].criteriaId;
												q.criteria_name = res[i].rows[j].criteriaName;
												q.question_id = res[i].rows[j].questionId;
												q.question_name = res[i].rows[j].questionName;
												q.question_weight = "";
												q.question_responce = res[i].rows[j].response;
												q.action_plan = res[i].rows[j].actionPlan;
												q.action_plan_date = res[i].rows[j].actionPlanDate;
												q.isValidated = res[i].rows[j].isValidated;

													ques[q.question_id] = q;

														if(q.isValidated!=1){
															cr = cr+1;
														}


										 }

													question[res[i].rows[0].criteriaId] = ques;

													criterias[res[i].rows[0].criteriaId] = res[i].rows[0].criteriaName;
									 }

								 }

							 }
							 ////console.log(BASE_URL)
							// alert(n)
							 //alert(cr)
							// var url =  BASE_URL + 'create';
							if(localStorage.getItem('USER_DESIGNATION')=='tsm'){
								var url =  BASE_URL + 'create';
							}else{
								 var url =  BASE_URL + 'updateSurvey';
							}

							 var params = {sid: sid, userid: localStorage.getItem('USER_ID'), dealer_code: localStorage.getItem('dealerCode'), survey_information: JSON.stringify(survey_information), norms: JSON.stringify(norms), norms: JSON.stringify(norms), [dep]: JSON.stringify(normArray), criterias: JSON.stringify(criterias), question: JSON.stringify(question)};
							 if(n==0 && cr==0){
								submitSurveyService.submitSurvey(url,params).then(function(result){
									////console.log(result)
									//alert(JSON.stringify(result))
									if(result.status){

										createSurvey.getAttachment().then(function(res){
										////console.log(res)


											if(res.rows && res.rows.length>0){
												  var promises = [];
												  angular.forEach(res.rows, function(val,key){
													promises.push($scope.uploadAttachment(val,result.attachmentsData));
												  });

												  $q.all(promises).then(function success(data){
														//update status in local DB
														var info = {};
														info.surveyId = localStorage.getItem('surveyId');
														info.referenceId = localStorage.getItem('referenceId');
														info.surveyStatusId = result.survey_status;
														info.serverSurveyId = result.survey_id;

														createSurvey.updateSurveyStatusOnSubmit(info).then(function(res){
														  ////console.log(res)
														  $ionicLoading.hide();
														  $ionicHistory.nextViewOptions({
															disableBack: true
														  });
														   //$state.go('app.dashboard');
														   $state.go('app.dashboard', {}, {reload: true});
														});
												  }, function failure(err){
													// Can handle this is we want
													alert("Error in uploading attachments.")
												  });
											}else{
												  //update status in local DB
												  //console.log(result)
												  var info = {};
												  info.surveyId = localStorage.getItem('surveyId');
												  info.referenceId = localStorage.getItem('referenceId');
												  info.surveyStatusId = result.survey_status;
												  info.serverSurveyId = result.survey_id;
												  createSurvey.updateSurveyStatusOnSubmit(info).then(function(res){
													////console.log(res)
													$ionicLoading.hide();
													//$state.go('app.dashboard');
													$state.go('app.dashboard', {}, {reload: true});
												  });
											}
										 });

									}else{
										alert("Failed to submit survey.");
									}
								}).catch(function (data) {
									//console.log(data)
									$ionicLoading.hide();
								});
							}else{

								if(n==0 && cr!=0){
									$scope.criteriaError = "All the criterias response are required.";
									$scope.normsError = "";
								}else if(n!=0 && cr==0){
									$scope.normsError = "All the norms data are required.";
									$scope.criteriaError = "";
								}else if(n!=0 && cr!=0){
									$scope.criteriaError = "All the criterias response are required.";
									$scope.normsError = "All the norms data are required.";
								}
								$ionicLoading.hide();
							}
							 /*  */
						 });

					});

			});
		}else{
				$ionicLoading.show();
				//get survey norms
				createSurvey.getNorms().then(function(res){
						////console.log(res.rows);
						if(res.rows.length>0){

							var data = res.rows;
							var n=0;
							for(var i=0;i<data.length;i++){
								//alert(JSON.stringify(data[i]))
								////console.log(data[i]);
								if(data[i].actual_value=="" || data[i].require_value=="" || !$scope.isNumeric(data[i].actual_value)){
									n = n+1;
									//alert(n)
								}
							}
						}
						 //get survey questions
						 createSurvey.getCriteria().then(function(res){
							 //console.log(res)
							 var cr = 0;
							 if(res.length>0){
								 for(var i=0;i<res.length;i++){
									 var ques = {};
									 if(res[i].rows.length>0){
										 for(var j=0;j<res[i].rows.length;j++){
											var q = {};
												q.criteria_id = res[i].rows[j].criteriaId;
												q.criteria_name = res[i].rows[j].criteriaName;
												q.question_id = res[i].rows[j].questionId;
												q.question_name = res[i].rows[j].questionName;
												q.question_weight = "";
												q.question_responce = res[i].rows[j].response;
												q.action_plan = res[i].rows[j].actionPlan;
												q.action_plan_date = res[i].rows[j].actionPlanDate;
												q.isValidated = res[i].rows[j].isValidated;
												//console.log(q.isValidated)
												ques[q.question_id] = q;
													if(q.isValidated!=1){
														cr = cr+1;
													}
										 }
									 }
								 }
							 }
							//alert(n)
							// alert(cr)
							 if(n==0 && cr==0){
								//update status to submitted but unsynced
								var info = {};
								info.surveyId = localStorage.getItem('surveyId');
								info.referenceId = localStorage.getItem('referenceId');

								createSurvey.updateSurveyStatusAfterSubmit(info).then(function(res){
									$ionicLoading.hide();
									$scope.criteriaError = "";
									$scope.normsError = "";
									$state.go('app.dashboard', {}, {reload: true});
								});
							 }else{

								if(n==0 && cr!=0){
									$scope.criteriaError = "All the criterias response are required.";
									$scope.normsError = "";
								}else if(n!=0 && cr==0){
									$scope.normsError = "All the norms data are required.";
									$scope.criteriaError = "";
								}else if(n!=0 && cr!=0){
									$scope.criteriaError = "All the criterias response are required.";
									$scope.normsError = "All the norms data are required.";
								}
								$ionicLoading.hide();
							}
						 });

					});
		}


	}
}
	//end survey submit
$scope.uploadAttachment = function(val,criteria){
//alert(val.criteriaId);
  var uploadUrl = BASE_URL + 'upload';
  var params = {};
  var criteria_submit_id="";
  var attachment_owner_id="";
  angular.forEach(criteria, function(v,key){
      if(val.criteriaId== v.criteria_id){
        criteria_submit_id = v.criteria_submit_id;
        attachment_owner_id = v.attachment_owner_id;
          params.criteriaIdSubmitted = criteria_submit_id;
          params.ownerId = attachment_owner_id;
          var options = {
                         fileKey: "import",
                         fileName: val.fileName,
                         chunkedMode: false,
                         mimeType: "image/jpg",
                         params : params
                  };
             $cordovaFileTransfer.upload(uploadUrl, val.file, options).then(function(result) {
                 ////console.log("SUCCESS: " + JSON.stringify(result));
                 //alert(JSON.stringify(result));
             }, function(err) {
                 ////console.log("ERROR: " + JSON.stringify(err));
                 //alert(err)
             }, function (progress) {
                 // constant progress updates
                 $timeout(function () {
                     $scope.downloadProgress = (progress.loaded / progress.total) * 100;
                 })
             });
      }
  });

  };
})
.controller('menuCtrl', function ($location, $ionicLoading, $ionicNavBarDelegate, $cordovaInAppBrowser, $scope, $http, $state, $ionicPopup, $ionicHistory) {
      /* var path = $location.path();
       alert(path)*/
       /*if (path === 'app/dashboard')
         $ionicNavBarDelegate.showBackButton(false);
        else
         $ionicNavBarDelegate.showBackButton(true);*/
	 $scope.$root.goToDashboard = function(){
		 //$ionicHistory.clearHistory();
		//$state.go($state.current, {}, {reload: true});
		$state.go('app.dashboard', {}, {reload: true});

	 }
	 $scope.$root.logout = function(){

				localStorage.setItem('isLoggedIn',false);
				localStorage.setItem('USER_ID',"");
				localStorage.setItem('USER_DESIGNATION',"");
				localStorage.setItem('LoginUser',"");
				localStorage.setItem('password',"");
				localStorage.setItem('rememberMe',"false");

				$state.go('login', {}, {reload: true});

	 }
           $scope.$root.LoginUser = localStorage.getItem('LoginUser');
         if(localStorage.getItem("loginUserDesignation")==='asm'){
            $scope.$root.mappingMenu = true;
         }else{
           $scope.$root.mappingMenu = false;
         }
         $scope.$on('$ionicView.beforeEnter', function (e, data) {
           //  //console.log(angular.element($(".left-buttons button")));
             if (data.stateName=='app.dashboard') {

                 $scope.$root.showMenuIcon = true;
                 $scope.$root.back = false;
				 angular.element($(".left-buttons button")).removeClass("hide");
             } else {
                 $scope.$root.showMenuIcon = false;
                 $scope.$root.back = true;
             }
         });
})
.controller('reportsCtrl', function ($location, $ionicNavBarDelegate, $cordovaInAppBrowser, $scope, $http, $state, $ionicPopup) {
	if(checkNetworkConnection()){

	}else{

	}
})
.controller('profileCtrl', function ($location, $ionicLoading, $cordovaSQLite, $ionicNavBarDelegate, $cordovaInAppBrowser, $scope, $http, $state, $ionicPopup, userAuthenticationService, userProfile) {
if(localStorage.getItem('USER_ID')=="" || localStorage.getItem('USER_ID')==null){
	$state.go('login');
}else{
	if(checkNetworkConnection()){
                    $ionicLoading.show();

					  var userId = localStorage.getItem('USER_ID');
                      var url = BASE_URL + 'profile?userId='+userId+'&designation_type='+localStorage.getItem('USER_DESIGNATION');
                      //var userData = {userId: userId, designation_type: 'tsm'};
						//alert(userId);
                      userAuthenticationService.profile(url, '').then(function (data) {
						  //console.log(data)
                        if (data != undefined || data != null) {
                          if(data.status){
							userProfile.updateProfile(data.user).then(function(){
								//console.log(data)
								$scope.empName = data.user.emp_name;
								$scope.empCode = data.user.emp_code;
								$scope.empEmail = data.user.emp_email_id;
								$scope.department = data.user.emp_department;

								if(data.user.status==1){
									$scope.active = true;
									$scope.inactive = false;
								}else{
									$scope.active = false;
									$scope.inactive = true;
								}
								$scope.contact = data.user.emp_primary_contact_no;
								$scope.areaOffice = data.user.emp_area_office_name;
								$scope.zone = data.user.emp_zone_name;
								$scope.reportToCode = data.user.emp_report_to_designation_code;
								$scope.reportTo = data.user.emp_report_to_name;
								$scope.reportToEmail = data.user.emp_report_to_person_email_id;
								$scope.dealers = data.user.dealers;
								$ionicLoading.hide();
							});
                          }else{
							$ionicLoading.hide();
                          }
                        } else {
                          //Close loader
						  $ionicLoading.hide();
                        }
                      }).catch(function (data) {
                        $scope.errorMsgLogin = 'Server error.';
                        $ionicLoading.hide();
                      });

    }else{
		$ionicLoading.show();
		var query = "SELECT * FROM profile INNER JOIN associated_dealers ON profile.emp_code=associated_dealers.emp_code WHERE profile.emp_code=9479";
		$cordovaSQLite.execute(db, query).then(function (result) {
			//console.log(result);
			if(result.rows.length>0){
					$scope.empName = result.rows[0].emp_name;
					$scope.empCode = result.rows[0].emp_code;
					$scope.empEmail = result.rows[0].emp_email_id;
					$scope.department = result.rows[0].emp_department;

					if(result.rows[0].status==1){
						$scope.active = true;
						$scope.inactive = false;
					}else{
						$scope.active = false;
						$scope.inactive = true;
					}
					$scope.contact = result.rows[0].emp_primary_contact_no;
					$scope.areaOffice = result.rows[0].emp_area_office_name;
					$scope.zone = result.rows[0].emp_zone_name;
					$scope.reportToCode = result.rows[0].emp_report_to_designation_code;
					$scope.reportTo = result.rows[0].emp_report_to_name;
					$scope.reportToEmail = result.rows[0].emp_report_to_person_email_id;
					//$scope.dealers = data.user.dealers;
					var dealers = [];

					for(var i=0;i<result.rows.length;i++){
						var d = {};
						////console.log(result.rows[i].dealer_code);
						d.dealer_code = result.rows[i].dealer_code;
						d.dealer_Name = result.rows[i].dealer_Name;
						dealers.push(d);
					}
					//console.log(dealers)
					$scope.dealers = dealers;
					$ionicLoading.hide();
			}else{
				alert("Sorry, no Internet connectivity detected. Please reconnect and try again.");
				$state.go('app.dashboard');
			}
		}, function (err) {
			console.error(err);
		});
    }
}
})
/* .controller('logoutCtrl', function ($location, $ionicNavBarDelegate, $cordovaInAppBrowser, $scope, $http, $state, $ionicPopup) {
    localStorage.setItem('isLoggedIn',false);
    localStorage.setItem('USER_ID',"");
    localStorage.setItem('USER_DESIGNATION',"");
	localStorage.setItem('LoginUser',"");
	localStorage.setItem('password',"");
	localStorage.setItem('rememberMe',"false");
}) */
/**
 @Name: remark
 @Type: Controller
 @Author: Anjani Kr Gupta
 @Date: 13th-Aug-2016
 */
.controller('mappingCtrl', function ($scope, $http, $state, $ionicLoading, mappingService, mapping, $ionicPopup) {
if(localStorage.getItem('USER_ID')=="" || localStorage.getItem('USER_ID')==null){
$state.go('login');
}else{
$scope.menu = false;
	if(checkNetworkConnection()){
		var response = [];
		var tsmList = [];
		var empcode = localStorage.getItem('empCode');
		var department = localStorage.getItem('department');
		//alert(department)
		var url = BASE_URL + 'getDealerListByAsm?empcode=' + empcode + '&department=' + department;
		$ionicLoading.show();
		mappingService.getMappingData(url, "").then(function (data) {
			console.log(data);
			if (data.status) {
				var associatedTSM = data.data[0]['Associated_TM'];
				var mappingData = data.data;
				////console.log(mappingData);
				for(var i=0;i<associatedTSM.length;i++){
					mapping.saveAssociatedTSM(associatedTSM[i], empcode);
					tsmList.push(associatedTSM[i]);
				}
				$scope.associatedTSM = tsmList;
				for(var i=0;i<mappingData.length;i++){
					mapping.saveMappingData(mappingData[i], empcode);
					response.push(mappingData[i]);
				}

				$scope.mappingData = response;
				////console.log(response)
				$ionicLoading.hide();
			} else {
				//Close loader
				$scope.formLoader = false;
				$ionicLoading.hide();
				$scope.errorMsg = "No tsm mapped to this asm";
			}
		}).catch(function (data) {
			alert(data)
			$ionicLoading.hide();
		});
	}else{
		var response = [];
		var tsmList = [];
		//get mapping data from local DB
		mapping.getAssociatedTSM(empcode).then(function (data) {
			//console.log(data)
			if(data.rows.length>0){
				for(var i=0;i<data.rows.length;i++){
					tsmList.push(data.rows[i]);
				}
				$scope.associatedTSM = tsmList;
				mapping.getMappingData(empcode).then(function (res) {
					if(res.rows.length>0){
						for(var i=0;i<res.rows.length;i++){
							response.push(res.rows[i]);
						}
						$scope.mappingData = response;
						////console.log(response)
					}else{
						alert("Sorry, no Internet connectivity detected. Please reconnect and try again.");
						$state.go('app.dashboard');
					}
				});
			}else{
				alert("Sorry, no Internet connectivity detected. Please reconnect and try again.");
				$state.go('app.dashboard');
			}

		});
	}
	var mappedObj = {};
	var params = {};
	var count=0;
	$scope.btnFlag = false;
	$scope.selectTSM = function(tsmEmpCode,dep,dealer){
		count++;
		var dataObj = {};
		$scope.btnFlag = true;
		for(var i=0;i<tsmList.length;i++){
			if(tsmEmpCode==tsmList[i]['emp_code']){
				dataObj.empName = tsmList[i]['emp_name'];
			}
		}
		dataObj.dealer_mapped_to_staff_person_code = tsmEmpCode;
		dataObj.is_dealer_mapped = 1;
		dataObj.dealer_mapped_to_staff_person_department_code = dep;
		dataObj.dealer_code = dealer;
		if(checkNetworkConnection()){
				dataObj.isSynched = 1;
		}else{
				dataObj.isSynched = 0;
		}
		mappedObj[dataObj.dealer_code] = dataObj;

	}

	$scope.saveDealerMapping = function(){
		var c=0;
		 if(checkNetworkConnection()){
			var url = BASE_URL + 'map_dealer';
			$ionicLoading.show();
			var params = {map: JSON.stringify(mappedObj)};
			mappingService.mapDealer(url, params).then(function (data) {
				//console.log(data)
				if(data.status){
					 for (var key in mappedObj) {
						mapping.updateDealerMapping(mappedObj[key],empcode).then(function(r){
								c++;
								if(count===c){
									//console.log("DATA UPDATED.");
									//$state.go('app.mapping');
									$ionicPopup.alert({
									 title: 'Dealer mapped successfully!',
									 template: ''
								   });
								   $ionicLoading.hide();
								}
						});
					}
					//$ionicLoading.hide();
				}else{
					$ionicPopup.alert({
									 title: data.message,
									 template: ''
								   });
					$ionicLoading.hide();
				}
			})
		}else{
			$ionicLoading.show();
			for (var key in mappedObj) {
				mapping.updateDealerMapping(mappedObj[key],empcode).then(function(r){
					//console.log(r)
						c++;
						if(count===c){
							$ionicLoading.hide();
							//console.log("LOCAL DATA UPDATED.");
							//$state.go('app.mapping');
							$ionicPopup.alert({
							 title: 'Dealer mapped successfully!',
							 template: ''
						   });
						}
				});
			}
		}
	}
}
})
