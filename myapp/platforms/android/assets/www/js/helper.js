function getCurrentDate(){
	var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }
    var today = dd+'/'+mm+'/'+yyyy;
	return today;
}

function generateReferenceId(month, year, code, empCode){
	var referenceId = empCode+''+code+month+year;
	return referenceId;
}

function generateMappingReferenceId(empCode, dealerCode){
	var referenceId = empCode+dealerCode;
	return referenceId;
}
/*
* Prepare survey information object
*/
function surveyBasicInfo(dealerCode, dealerName, department, month, year, surveyId, empCode, referenceId){
	var info = {};
/* 	1 =>	Draft
	2 => 	Submitted
	3 =>	approved_by_asm
	4 =>	approved_by_zo */
	if(department=="sales"){
		var dep = 1;
	}else if(department=="services"){
		var dep = 2;
	}else{
		var dep = undefined;
	}
	info.surveyId = surveyId;
	info.referenceId = referenceId;
	info.survey_name = "survey for " + dealerName;
	info.survey_status_id = Draft; //update status on form submit
	info.asso_dealer_code = dealerCode;
	info.survey_for_dept_code = dep;
	info.survey_owner_emp_code = empCode;
	info.survey_created_date = getCurrentDate();
	info.survey_visited_date = getCurrentDate();
	info.survey_modified_date = getCurrentDate();
	info.survey_month = month;
	info.survey_year = year;
	info.survey_remark = "";
	info.survey_remark_asm = "";
	info.survey_remark_zo = "";
	info.flag = 2;
	return info;
}
/*
* Prepare sales norms object
*/
function salesNorms(data){
	var res = [];

	for (var key in data) {
			var norms = {};
			if (data.hasOwnProperty(key)) {
				//console.log(key)
				norms.designationAlias = key;
				norms.designation = data[key]['designation']['#markup'];
				norms.required_value = data[key]['required_value']['#markup'];
					if(data[key]['required_value'].hasOwnProperty('#markup')){
						norms.required_value = data[key]['required_value']['#markup'];
						norms.required_value_type = "not-editable";
					}else{
						norms.required_value_type = "editable";
						norms.required_value = "";
					}
				norms.actual_value = "";
				norms.action_plan = "";
				norms.action_plan_asm = "";
				norms.action_plan_zm = "";
				norms.target_date = "";
				norms.flag = 2;
			}
			res.push(norms);
	}
	return res;
}

function groupBy( array , f )
{
  var groups = {};
  array.forEach( function( o )
  {
    var group = JSON.stringify( f(o) );
    groups[group] = groups[group] || [];
    groups[group].push( o );
  });
  return Object.keys(groups).map( function( group )
  {
    return groups[group];
  })
}
function getLastThreeMonth()
{
	var months = {};
	/* var monthNames = [ "January", "February", "March", "April", "May", "June",
	"July", "August", "September", "October", "November", "December" ]; */
	var d = new Date();
	var currentMonth = d.getMonth()+1;
	var prevMonth = currentMonth-1;
	var prevToPrevMonth = prevMonth-1;
	months[currentMonth] = monthNames[currentMonth-1];
	months[prevMonth] = monthNames[prevMonth-1];
	months[prevToPrevMonth] = monthNames[prevToPrevMonth-1];
	return months;
}
function getLastThreeYear()
{
	              var years = {};
               	var d = new Date();
               	var currentYear = d.getFullYear();
               	var prevYear = currentYear-1;
               	var prevToPrevYear = prevYear-1;
               	years[currentYear] = currentYear;
               	//years[prevYear] = prevYear;
               	//years[prevToPrevYear] = prevToPrevYear;
               	return years;
}
function checkNetworkConnection(){
//alert(navigator.connection.type);
    /*if(window.Connection) {
        if(navigator.connection.type == Connection.NONE) {
          *//* $ionicPopup.confirm({
            title: 'No Internet Connection',
            content: 'Sorry, no Internet connectivity detected. Please reconnect and try again.'
          })
          .then(function(result) {
            if(!result) {
              ionic.Platform.exitApp();
            }
          });*//*
          return false;
        }else{
          return true;
        }
   }else{
    return false;
   }*/
   if(navigator.connection.type == 'none') {
      return false;
   }else{
      return true;
   }
   //return true;
}
function formStatus(survey_status){
  var response = {};
  response.status = false;
  response.btnTxt = "Submit";
  response.btnStatus = false;
  if(USER_DESIGNATION=='tsm'){
      if(survey_status!='Start' && survey_status!='Draft'){
          response.status = true;
          response.btnStatus = true;
      }
  }else if(USER_DESIGNATION=='asm'){
       if(survey_status!='Waiting For Approval'){
          response.status = true;
          response.btnStatus = true;
       }else{
         response.status = false;
         response.btnTxt = "Approve";
       }
  }else if(USER_DESIGNATION=='zo'){
      if(survey_status!='Waiting For Approval'){
          response.status = true;
          response.btnStatus = true;
      }else{
          response.status = false;
          response.btnTxt = "Approve";
       }
  }
  return response;
}
