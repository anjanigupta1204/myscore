angular.module('starter.services', [])
/**
	@Name: userAuthenticationService
	@Type: Services
	@Author: Anjani Kr Gupta
	@Date: 4-Jul-2016
*/
.service('userAuthenticationService', function ($state, $http, $q) {
  var httpService = {};
	httpService.login = function(_url, _data){
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: _url,
			data: _data,
		headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
			cache: false,

		}).success(function(data, status, headers, config){
			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}

	httpService.logout = function(_url){
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: _url,
			headers: {
                'Content-Type': 'application/json'
            },
			cache: false,

		}).success(function(data, status, headers, config){
			deferred.resolve(data);
		})
		.error(function(data, status, headers, config){
			deferred.reject(data);
		});
		return deferred.promise;
	}
	httpService.profile = function(_url, _data){
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: _url,
			data: _data,
			/* headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }, */
			cache: false,

		}).success(function(data, status, headers, config){
			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}
  return httpService;
})

//Dashboard Service
.service('getDealerService', function ($state, $http,$q) {
  var httpService = {};
	httpService.getDealers = function(_url, _data){
		var deferred =  $q.defer();
		$http({
			method: 'GET',
			url: _url,
			data: _data,
/* 			headers: {
               'Content-Type': 'application/json'
            }, */
			cache: false,

		}).success(function(data, status, headers, config){

			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}

  return httpService;
})
//Guideline service
.service('guideLine', function ($state, $http, $q) {
  var httpService = {};
	httpService.getGuideLine = function(_url, _data){
		var deferred =  $q.defer();
		$http({
			method: 'GET',
			url: _url,
			data: _data,
/* 			headers: {
               'Content-Type': 'application/json'
            }, */
			cache: false,

		}).success(function(data, status, headers, config){

			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}
	 return httpService;
})
//Sales Data Service
.service('getSalesDataService', function ($state, $http,$q) {
  var httpService = {};
	httpService.getSalesData = function(_url, _data){
		var deferred =  $q.defer();
		$http({
			method: 'GET',
			url: _url,
			data: _data,
/* 			headers: {
               'Content-Type': 'application/json'
            }, */
			cache: false,

		}).success(function(data, status, headers, config){

			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}
	 return httpService;
})

//Sales norms Service
.service('getNormsService', function ($state, $http, $q) {
  var httpService = {};
	httpService.getNorms = function(_url, _data){
		var deferred =  $q.defer();
		$http({
			method: 'GET',
			url: _url,
			data: _data,
/* 			headers: {
               'Content-Type': 'application/json'
            }, */
			cache: false,

		}).success(function(data, status, headers, config){

			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}
	 return httpService;
})
//Sales norms Service
.service('getQuestionsService', function ($state, $http, $q) {
  var httpService = {};
	httpService.getQuestions = function(_url, _data){
		var deferred =  $q.defer();
		$http({
			method: 'GET',
			url: _url,
			data: _data,
/* 			headers: {
               'Content-Type': 'application/json'
            }, */
			cache: false,

		}).success(function(data, status, headers, config){

			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}
	 return httpService;
})
//Survey remarks
.service('remarks', function ($state, $http, $q) {
  var httpService = {};
	httpService.getRemarks = function(_url, _data){
		var deferred =  $q.defer();
		$http({
			method: 'GET',
			url: _url,
			data: _data,
/* 			headers: {
               'Content-Type': 'application/json'
            }, */
			cache: false,

		}).success(function(data, status, headers, config){

			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}
	 return httpService;
})
.service('submitSurveyService', function ($state, $http, $q) {
  var httpService = {};
	httpService.submitSurvey = function(_url, _data){
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: _url,
			data: _data,
			 headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
			cache: false,

		}).success(function(data, status, headers, config){
			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}
	 return httpService;
})
.service('mappingService', function ($state, $http, $q) {
  var httpService = {};
	httpService.getMappingData = function(_url, _data){
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: _url,
			data: _data,
			 /* headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },  */
			cache: false,

		}).success(function(data, status, headers, config){
			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}
	httpService.mapDealer = function(_url, _data){
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: _url,
			data: _data,
			headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
			cache: false,

		}).success(function(data, status, headers, config){
			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}
	 return httpService;
})

.factory('Camera', function($q) {

   return {
      getPicture: function(options) {
         var q = $q.defer();

         navigator.camera.getPicture(function(result) {
            q.resolve(result);
         }, function(err) {
            q.reject(err);
         }, options);

         return q.promise;
      }
   }

});
