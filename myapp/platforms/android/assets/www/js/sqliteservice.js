angular.module('starter.sqliteservices', [])


.factory('DBA', function($cordovaSQLite, $q, $ionicPlatform, $rootScope) {
  var self = this;

  // Handle query's and potential errors
  self.query = function (query, parameters) {
    parameters = parameters || [];
    var q = $q.defer();

    $ionicPlatform.ready(function () {
      $cordovaSQLite.execute(db, query, parameters)
        .then(function (result) {
          q.resolve(result);
        }, function (error) {
          console.warn('I found an error');
          console.warn(error);
          q.reject(error);
        });
    });
    return q.promise;
  }

  // Proces a result set
  self.getAll = function(result) {
    var output = [];

    for (var i = 0; i < result.rows.length; i++) {
      output.push(result.rows.item(i));
    }
    return output;
  }

  // Proces a single result
  self.getById = function(result) {
    var output = null;
    output = angular.copy(result.rows.item(0));
    return output;
  }

  return self;
})

.factory('DEALERS', function($cordovaSQLite, DBA) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM dealers")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(memberId) {
    var parameters = [memberId];
    return DBA.query("SELECT * FROM dealers")
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(data,stats) {
	 // //console.log(stats)
	 //$cordovaSQLite.execute(db, "DROP TABLE dealers");
		$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS survey_criteria_attachment (surveyId numeric, referenceId numeric, criteriaId text,fileName text, file text, isSynched numeric ,modificationDate text, creationDate text)");
		$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS dealers (surveyId integer primary key, referenceId numeric, empCode text, empName text,empDesignation text, empDepartment text,empLoginId text,empAreaOffice text,empAreaOfficeCode text,empZone text,empZoneCode text,dealerCode numeric,dealerName text,dealerAreaOfficeCode text,dealerLocation text,dealerMappedStaff text,dealerMappedStaffDepartment text,month numeric,year numeric, modificationDate text, surveyStatus text, surveyStatusASM text, surveyStatusZO text, tsmId numeric, asmId numeric, zoId numeric, serverSid numeric, creationDate text)");

	var referenceId = generateReferenceId(data.month, data.year, data.dealerCode, parseInt(data.empCode));
	////console.log(referenceId)
    var parameters = [referenceId, data.empCode, data.empName, data.empDesignation, data.empDepartment, data.empLoginId, data.empAreaOffice,data.empAreaOfficeCode,data.empZone ,data.empZoneCode ,data.dealerCode ,data.dealerName ,data.dealerAreaOfficeCode ,data.dealerLocation ,data.dealerMappedStaff ,data.dealerMappedStaffDepartment, data.month, data.year, data.surveyStatus, localStorage.getItem('USER_ID'), getCurrentDate()];
				//add check if data is already exists.
				return DBA.query("SELECT * FROM dealers WHERE month ="+data.month+" AND year="+data.year+" AND dealerCode="+data.dealerCode+" AND referenceId="+referenceId).then(function(res) {
					////console.log(res)
					if(res.rows.length>0){
						//update value
						//return res;

						if(stats.loggedInEmpDesignation =='tsm'){
												  if(checkNetworkConnection()){
													  //console.log(res.rows[0].surveyStatus)
													  if(res.rows[0].surveyStatus!="Draft" && res.rows[0].surveyStatus!="Submitted_but_unsynced"){
														  var params = [data.surveyStatus, localStorage.getItem('USER_ID'), data.month, data.year, res.rows[0].surveyId, res.rows[0].referenceId];
																   return DBA.query("UPDATE dealers SET surveyStatus = (?), tsmId=(?) WHERE month=(?) AND year=(?) AND surveyId = (?) AND referenceId = (?)", params);
													  }else{
														  var params = [localStorage.getItem('USER_ID'), data.month, data.year, res.rows[0].surveyId, res.rows[0].referenceId];
																   return DBA.query("UPDATE dealers SET tsmId=(?) WHERE month=(?) AND year=(?) AND surveyId = (?) AND referenceId = (?)", params);
													  }
												  }else{
													  if(res.rows[0].surveyStatus!="Draft" && res.rows[0].surveyStatus!="Submitted_but_unsynced"){
														  var params = [data.surveyStatus, localStorage.getItem('USER_ID'), data.month, data.year, res.rows[0].surveyId, res.rows[0].referenceId];
														  return DBA.query("UPDATE dealers SET surveyStatus = (?), tsmId=(?) WHERE month=(?) AND year=(?) AND surveyId = (?) AND referenceId = (?)", params);
													  }else{
														  var params = [localStorage.getItem('USER_ID'), data.month, data.year, res.rows[0].surveyId, res.rows[0].referenceId];
														  return DBA.query("UPDATE dealers SET tsmId=(?) WHERE month=(?) AND year=(?) AND surveyId = (?) AND referenceId = (?)", params);
													  }
												  }
									}else if(stats.loggedInEmpDesignation =='asm'){
										////console.log('asm')
										var params = [data.surveyStatus, localStorage.getItem('USER_ID'), data.month, data.year, res.rows[0].surveyId, res.rows[0].referenceId];
										return DBA.query("UPDATE dealers SET surveyStatusASM = (?), asmId=(?) WHERE month=(?) AND year=(?) AND surveyId = (?) AND referenceId = (?)", params);
									}else if(stats.loggedInEmpDesignation =='zo'){
										var params = [data.surveyStatus, localStorage.getItem('USER_ID'), data.month, data.year, res.rows[0].surveyId, res.rows[0].referenceId];
						  return DBA.query("UPDATE dealers SET surveyStatusZO = (?), zoId=(?) WHERE month=(?) AND year=(?) AND surveyId = (?) AND referenceId = (?)", params);
						}else{
							var params = [data.surveyStatus, localStorage.getItem('USER_ID'), data.month, data.year, res.rows[0].surveyId, res.rows[0].referenceId];
						  return DBA.query("UPDATE dealers SET surveyStatus = (?), tsmId=(?) WHERE month=(?) AND year=(?) AND surveyId = (?) AND referenceId = (?)", params);
						}
					}else{
						if(stats.loggedInEmpDesignation =='tsm'){
						  return DBA.query("INSERT INTO dealers (referenceId, empCode, empName, empDesignation, empDepartment, empLoginId, empAreaOffice, empAreaOfficeCode, empZone ,empZoneCode ,dealerCode ,dealerName ,dealerAreaOfficeCode ,dealerLocation ,dealerMappedStaff ,dealerMappedStaffDepartment,month,year,surveyStatus,tsmId, creationDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
						}else if(stats.loggedInEmpDesignation =='asm'){
						  return DBA.query("INSERT INTO dealers (referenceId, empCode, empName, empDesignation, empDepartment, empLoginId, empAreaOffice, empAreaOfficeCode, empZone ,empZoneCode ,dealerCode ,dealerName ,dealerAreaOfficeCode ,dealerLocation ,dealerMappedStaff ,dealerMappedStaffDepartment,month,year,surveyStatusASM,asmId, creationDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
						}else if(stats.loggedInEmpDesignation =='zo'){
						  return DBA.query("INSERT INTO dealers (referenceId, empCode, empName, empDesignation, empDepartment, empLoginId, empAreaOffice, empAreaOfficeCode, empZone ,empZoneCode ,dealerCode ,dealerName ,dealerAreaOfficeCode ,dealerLocation ,dealerMappedStaff ,dealerMappedStaffDepartment,month,year,surveyStatusZO,zoId, creationDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
						}else{
						  return DBA.query("INSERT INTO dealers (referenceId, empCode, empName, empDesignation, empDepartment, empLoginId, empAreaOffice, empAreaOfficeCode, empZone ,empZoneCode ,dealerCode ,dealerName ,dealerAreaOfficeCode ,dealerLocation ,dealerMappedStaff ,dealerMappedStaffDepartment,month,year,surveyStatus,tsmId, creationDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
						}
					}
				});

  }
	self.addStats = function(data) {
		console.log(data)
		 return DBA.query("CREATE TABLE IF NOT EXISTS survey_stats (loggedInEmpId numeric, loggedInEmpCode numeric, empLoggedInDesignation text, completed numeric, pending numeric, modification_date text,creation_date text)").then(function(){

			    var parameters = [data.empId, data.empCode, data.loggedInEmpDesignation, data.completed, data.pending, getCurrentDate()];
				//add check if data is already exists.
				return DBA.query("SELECT * FROM survey_stats WHERE loggedInEmpCode="+data.empCode).then(function(res) {
					////console.log(res)
					if(res.rows.length>0){
						//update value
						return DBA.query("UPDATE survey_stats SET loggedInEmpId=(?), loggedInEmpCode = (?),  empLoggedInDesignation = (?), completed = (?), pending = (?) , modification_date = (?)", parameters);
					}else{
						    return DBA.query("INSERT INTO survey_stats (loggedInEmpId, loggedInEmpCode , empLoggedInDesignation , completed , pending , creation_date) VALUES (?,?,?,?,?,?)", parameters);
					}
				});
		 });
  }
  self.remove = function(member) {
    var parameters = [member.id];
    return DBA.query("DELETE FROM dealers WHERE id = (?)", parameters);
  }

  self.update = function(origMember, editMember) {
    var parameters = [editMember.id, editMember.name, origMember.id];
    return DBA.query("UPDATE dealers SET id = (?), name = (?) WHERE id = (?)", parameters);
  }

  return self;
})
/*
* Method: survey
* Description: To perform CRUD operation for survey
* Date: 22nd July 2016
*/
.factory('survey', function($cordovaSQLite,$rootScope, DBA, $q) {
  var self = this;

   self.getGuideLineData = function(data) {
	return DBA.query("SELECT * FROM survey_basic_info SBI INNER JOIN dealers DEL ON DEL.referenceId=SBI.referenceId WHERE SBI.surveyId="+data.surveyId+" AND SBI.referenceId="+data.referenceId)
	  .then(function(result) {
		return result;
	  });
	}
   self.getSurveySalesData = function() {
	var query = "SELECT SD.sales_data_type, SD.sales_data_sub_type" +
	" FROM s_sales_data SD WHERE SD.surveyId="+localStorage.getItem('surveyId')+" GROUP BY "+
	"SD.sales_data_sub_type";
    return DBA.query(query)
      .then(function(result) {
		return self.getSurveySalesSubData(result.rows);
		// return result;
      });
  }
  self.getSurveySalesSubData = function(data) {
		var promises = [];
		for(var i=0;i<data.length;i++){
				var salesDataType = data[i].sales_data_type;
				var salesDataSubType = data[i].sales_data_sub_type;
				var query = "SELECT * FROM s_sales_data WHERE sales_data_type='"+salesDataType+ "' AND sales_data_sub_type='"+salesDataSubType+"' AND surveyId ="+localStorage.getItem('surveyId');
				promises.push(DBA.query(query));
			}
	 return $q.all(promises);
 }
  self.getRemarks = function(id) {
    return DBA.query("SELECT * FROM s_remark WHERE surveyId="+localStorage.getItem('surveyId')+ " AND referenceId="+localStorage.getItem('referenceId'))
      .then(function(result) {
        return result;
      });
  }

  self.getSurveyNorms = function(surveyId,referenceId) {
    return DBA.query("SELECT * FROM survey_norms  WHERE "+
	" surveyId="+surveyId+" AND referenceId="+referenceId)
      .then(function(result) {
        return result;
      });
  }
  self.getSurveyNorm = function(alias) {
    return DBA.query("SELECT SN.designation designation, SN.alias alias, SN.actual_value actualValue,  SN.required_value_type requiredValueType, SN.require_value requiredValue, SN.action_plan actionPlan, SN.action_plan_asm actionPlanAsm, SN.action_plan_zm actionPlanZm, SN.target_date targetDate, sns.action_plan actionPlanStatus, sns.action_plan_asm actionPlanAsmStatus, sns.action_plan_zm actionPlanZmStatus  FROM survey_norms SN INNER JOIN s_norms sns ON SN.alias=sns.alias WHERE "+
	" SN.surveyId="+localStorage.getItem("surveyId")+ " AND SN.alias='"+alias+ "' AND SN.referenceId="+localStorage.getItem("referenceId"))
      .then(function(result) {
        return result;
      });
  }

  self.getCriteria = function() {
    return DBA.query("SELECT criteriaId, criteriaName, isValidated FROM s_criteria_questions WHERE " +
	" surveyId="+localStorage.getItem("surveyId")+ " AND referenceId="+localStorage.getItem("referenceId")+ " GROUP BY criteriaId")
      .then(function(result) {
        return result;
      });
  }
    self.getCriteriaQuestions = function(crId) {
    return DBA.query("SELECT scq.criteriaId, scq.criteriaName, scq.questionId, scq.questionName, scq.actionPlan prvActionPlan, scq.actionPlanAsm prvActionPlanAsm, scq.actionPlanZm prvActionPlanZm, scq.actionPlanStatus actionPlanStatus, scq.actionPlanAsmStatus actionPlanAsmStatus, scq.actionPlanZmStatus actionPlanZmStatus, sqa.response response, sqa.actionPlan actionPlan, sqa.actionPlanAsm actionPlanAsm, sqa.actionPlanZm actionPlanZm, sqa.actionPlanDate actionPlanDate FROM s_criteria_questions scq INNER JOIN survey_quetions_answers sqa ON scq.questionId=sqa.questionId  WHERE sqa.surveyId="+localStorage.getItem('surveyId')+" AND scq.surveyId="+localStorage.getItem('surveyId')+" AND scq.referenceId="+localStorage.getItem('referenceId')+ " AND sqa.referenceId="+localStorage.getItem('referenceId')+" AND scq.criteriaId="+crId+" AND sqa.criteriaId="+crId)
      .then(function(result) {
        return result;
      });
  }
  self.addSalesData = function(salesData) {
	 // //console.log(salesData)
	 return DBA.query("CREATE TABLE IF NOT EXISTS s_sales_data (surveyId numeric,  referenceId numeric, durationtype text,month text, year text,sales_data_sub_type text,sales_data_type text ,Tgt text,Ach text, percentage_Ach text,LY text, percentage_Gr text,modification_date text,creation_date text)").then(function(){
		 return DBA.query("SELECT * FROM s_sales_data WHERE surveyId="+localStorage.getItem("surveyId")+" AND referenceId="+localStorage.getItem("referenceId")).then(function(res) {
			if(res.rows.length>0){
				//update value
				return res;
			}else{
				//add value
				var sales =   salesData.Sales;
				var services = salesData.Services;
				////console.log(sales);
				for (var key in sales) {
				  if (sales.hasOwnProperty(key)) {
					  for (var keyData in sales[key].Monthly) {
						   if (sales[key].Monthly.hasOwnProperty(keyData)) {
							   ////console.log(sales[key].Monthly['Tgt']);
							   var parameters = [localStorage.getItem("surveyId"),localStorage.getItem("referenceId"),"Monthly","08","2016",key,"sales",sales[key].Monthly['Tgt'],sales[key].Monthly['Ach'],sales[key].Monthly['%Ach'],sales[key].Monthly['LY'],sales[key].Monthly['%Gr'],getCurrentDate()];
								DBA.query("INSERT INTO s_sales_data (surveyId,referenceId,durationtype,month , year ,sales_data_sub_type, sales_data_type ,Tgt ,Ach ,percentage_Ach ,LY ,percentage_Gr ,creation_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
						   }
					  }
					 for (var keyData in sales[key].YTM) {
						   if (sales[key].YTM.hasOwnProperty(keyData)) {
							   ////console.log(sales[key].YTM['Tgt']);
							   var parameters = [localStorage.getItem("surveyId"),localStorage.getItem("referenceId"),"YTM","08","2016",key,"sales",sales[key].YTM['Tgt'],sales[key].YTM['Ach'],sales[key].YTM['%Ach'],sales[key].YTM['LY'],sales[key].YTM['%Gr'],getCurrentDate()];
								DBA.query("INSERT INTO s_sales_data (surveyId,referenceId,durationtype,month , year ,sales_data_sub_type, sales_data_type ,Tgt ,Ach ,percentage_Ach ,LY ,percentage_Gr ,creation_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
						   }
					  }
				  }
				 }

				for (var key in services) {
				  if (services.hasOwnProperty(key)) {

					  for (var keyData in services[key].Monthly) {
						   if (services[key].Monthly.hasOwnProperty(keyData)) {
							  // //console.log(sales[key].Monthly['Tgt']);
							   var parameters = [localStorage.getItem("surveyId"),localStorage.getItem("referenceId"),"Monthly","08","2016",key,"services",services[key].Monthly['Tgt'],services[key].Monthly['Ach'],services[key].Monthly['%Ach'],services[key].Monthly['LY'],services[key].Monthly['%Gr'],getCurrentDate()];
								DBA.query("INSERT INTO s_sales_data (surveyId,referenceId,durationtype,month , year ,sales_data_sub_type, sales_data_type ,Tgt ,Ach ,percentage_Ach ,LY ,percentage_Gr ,creation_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
						   }
					  }

					  for (var keyData in services[key].YTM) {
						   if (services[key].YTM.hasOwnProperty(keyData)) {
							  // //console.log(sales[key].YTM['Tgt']);
							   var parameters = [localStorage.getItem("surveyId"),localStorage.getItem("referenceId"),"YTM","08","2016",key,"services",services[key].YTM['Tgt'],services[key].YTM['Ach'],services[key].YTM['%Ach'],services[key].YTM['LY'],services[key].YTM['%Gr'],getCurrentDate()];
								DBA.query("INSERT INTO s_sales_data (surveyId,referenceId,durationtype,month , year ,sales_data_sub_type, sales_data_type ,Tgt ,Ach ,percentage_Ach ,LY ,percentage_Gr ,creation_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
						   }
					  }

				  }
				 }
			}

		 },function (err) {
			 return err;
		});

	  });

  }

  self.addNorms = function(norms,nKey) {
	  console.log(norms)
	return DBA.query("CREATE TABLE IF NOT EXISTS s_norms (surveyId numeric, referenceId numeric, alias text, designation text, required_value text, actual_value text, target_date text, action_plan text,action_plan_asm text,action_plan_zm text, modificationDate text, creationDate text)").then(function(){
		 return DBA.query("SELECT * FROM s_norms WHERE surveyId="+localStorage.getItem("surveyId")+" AND referenceId="+localStorage.getItem("referenceId")).then(function(res) {
			if(res.rows.length>0){
				//update value
				return res;
			}else{
				//add value
				if(nKey=='sales'){
					for (var key in norms) {
						//console.log(norms)
							if (norms.hasOwnProperty(key)) {
								//console.log(key)
								if(norms[key]['required_value'].hasOwnProperty('#markup')){
									var rv = norms[key]['required_value']['#markup'];
								}else{
									var rv = "textfield";
								}
								var parameters = [localStorage.getItem("surveyId"),localStorage.getItem("referenceId"),key,norms[key]['designation']['#markup'],rv,norms[key]['actual_status']['#required'],norms[key]['action_plan']['#disabled'],norms[key]['action_plan_asm']['#disabled'],norms[key]['action_plan_zm']['#disabled'],getCurrentDate()];
								DBA.query("INSERT INTO s_norms (surveyId, referenceId, alias , designation , required_value , actual_value ,  action_plan ,action_plan_asm ,action_plan_zm ,  creationDate ) VALUES (?,?,?,?,?,?,?,?,?,?)", parameters);
							}
					}
				}else{
					for (var key in norms) {
						//console.log(norms)
							if (norms.hasOwnProperty(key)) {
								//console.log(key)
								if(norms[key]['current_detail'].hasOwnProperty('#markup')){
									var rv = norms[key]['current_detail']['#markup'];
								}else{
									var rv = "textfield";
								}
								var parameters = [localStorage.getItem("surveyId"),localStorage.getItem("referenceId"),key,norms[key]['designation']['#markup'],rv,norms[key]['actual_status']['#required'],norms[key]['action_plan']['#disabled'],norms[key]['action_plan_asm']['#disabled'],norms[key]['action_plan_zm']['#disabled'],getCurrentDate()];
								DBA.query("INSERT INTO s_norms (surveyId, referenceId, alias , designation , required_value , actual_value ,  action_plan ,action_plan_asm ,action_plan_zm ,  creationDate ) VALUES (?,?,?,?,?,?,?,?,?,?)", parameters);
							}
					}
				}

			}
		 },function (err) {
			 return err;
		});
	});
  }

  self.addCriteriaQuetions = function(data,cr) {
	return DBA.query("CREATE TABLE IF NOT EXISTS s_criteria_questions (surveyId numeric, referenceId numeric, criteriaId numeric, criteriaName text, questionId numeric, questionName text, actionPlan text, actionPlanAsm text, actionPlanZm text, actionPlanStatus text, actionPlanAsmStatus text, actionPlanZmStatus text, actionPlanDate text, isValidated numeric, isSynched numeric, modificationDate text, creationDate text)").then(function(){
			return DBA.query("CREATE TABLE IF NOT EXISTS survey_quetions_answers (surveyId numeric, referenceId numeric, criteriaId numeric,  questionId numeric, response text, actionPlan text, actionPlanAsm text, actionPlanZm text, actionPlanDate text, isValidated numeric, isSynched numeric, modificationDate text, creationDate text)").then(function(){

						DBA.query("SELECT * FROM s_criteria_questions WHERE surveyId="+localStorage.getItem("surveyId")+" AND referenceId="+localStorage.getItem("referenceId") + " AND criteriaId IN ("+cr+")" ).then(function(res) {
							if(res.rows.length>0){
								//update value

							}else{
								//add value
								for (var i=0;i<data.length;i++) {

									var parameters = [localStorage.getItem('surveyId'), localStorage.getItem('referenceId'), data[i].criteriaId, data[i].criteriaName, data[i].questionId, data[i].questionName, data[i].actionPlan, data[i].actionPlanAsm, data[i].actionPlanZm,data[i].actionPlanStatus,data[i].actionPlanAsmStatus,data[i].actionPlanZmStatus, getCurrentDate(), '0', '0' ,getCurrentDate()];
									DBA.query("INSERT INTO s_criteria_questions (surveyId, referenceId, criteriaId, criteriaName, questionId, questionName, actionPlan, actionPlanAsm, actionPlanZm, actionPlanStatus, actionPlanAsmStatus, actionPlanZmStatus, actionPlanDate, isValidated, isSynched, creationDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);

								}
							}
						});
						DBA.query("SELECT * FROM survey_quetions_answers WHERE surveyId="+localStorage.getItem("surveyId")+" AND referenceId="+localStorage.getItem("referenceId") + " AND criteriaId IN ("+cr+")" ).then(function(res) {
								if(res.rows.length>0){
									//update value
								}else{
									//add value
									for (var i=0;i<data.length;i++) {

										var params = [localStorage.getItem('surveyId'), localStorage.getItem('referenceId'), data[i].criteriaId, data[i].questionId, 'false', "", "", "",  "", '0', '0' ,getCurrentDate()];
										DBA.query("INSERT INTO survey_quetions_answers (surveyId, referenceId, criteriaId, questionId, response, actionPlan, actionPlanAsm, actionPlanZm, actionPlanDate, isValidated, isSynched, creationDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", params);
									}
								}
						});
			});
	});
  }

  self.addRemark = function(data) {
	return DBA.query("CREATE TABLE IF NOT EXISTS s_remark (title text, description text, disabled text, surveyId numeric, referenceId numeric, isSynched numeric, modificationDate text, creationDate text)").then(function(){
		////console.log(localStorage.getItem('surveyId'))
		return DBA.query("SELECT * FROM s_remark WHERE surveyId="+localStorage.getItem('surveyId')+" AND referenceId="+localStorage.getItem('referenceId')).then(function(res) {
			////console.log(res)
					if(res.rows.length>0){
						//update value
						return res;
					}else{
						//add value
						for(var i=0;i<data.length;i++){
							var parameters = [data[i].title, data[i].defaultValue, data[i].disabled, localStorage.getItem('surveyId'), localStorage.getItem('referenceId'), 0, getCurrentDate()];
							DBA.query("INSERT INTO s_remark (title, description, disabled, surveyId, referenceId, isSynched, creationDate ) VALUES (?,?,?,?,?,?,?)", parameters);
						}
					}
		});
	});
  }
  self.updateSurveyBasicInfo = function(data) {
    var parameters = [data.guideLine, data.referenceId, data.surveyId];
    return DBA.query("UPDATE survey_basic_info SET guideline = (?) WHERE referenceId = (?) AND surveyId = (?)", parameters);
  }

  self.updateSurveyBasicInfoVisitDate = function(data) {
    var parameters = [data.visitDate, data.referenceId, data.surveyId];
    return DBA.query("UPDATE survey_basic_info SET survey_visited_date = (?) WHERE referenceId = (?) AND surveyId = (?)", parameters);
  }

  return self;
})

/*
* Author: Anjani Kr. Gupta
* Method: createSurvey
* Description: create survey and save all survey data locally
* Date: 5th Aug 2016
*/

.factory('createSurvey', function($cordovaSQLite, $rootScope, DBA, $q) {
  var self = this;

  self.submitSurveyBasicInfo = function(surveyInfo) {
  //console.log(surveyInfo);
    return DBA.query("CREATE TABLE IF NOT EXISTS survey_basic_info (surveyId numeric, referenceId numeric, survey_name text, survey_status_id numeric, asso_dealer_code numeric, survey_for_dept_code numeric, survey_owner_emp_code numeric, survey_created_date text, survey_visited_date text, survey_modified_date text, survey_month numeric, survey_year numeric, survey_remark text, survey_remark_asm text, survey_remark_zo text, survey_score_percent numeric, auto_approval_status numeric, guideline text, flag numeric, isSynched numeric ,serverSid numeric, modificationDate text, creationDate text)").then(function(result) {
			//add check if data is already exists.
				return DBA.query("SELECT * FROM survey_basic_info WHERE surveyId="+surveyInfo.surveyId+" AND referenceId="+surveyInfo.referenceId).then(function(res) {

					if(res.rows.length>0){
						//update value
						return res;
					}else{
						//add value
						var parameters = [surveyInfo.surveyId, surveyInfo.referenceId, surveyInfo.survey_name, surveyInfo.survey_status_id, surveyInfo.asso_dealer_code, surveyInfo.survey_for_dept_code, surveyInfo.survey_owner_emp_code, surveyInfo.survey_created_date, surveyInfo.survey_visited_date, surveyInfo.survey_modified_date, surveyInfo.survey_month, surveyInfo.survey_year, surveyInfo.survey_remark, surveyInfo.survey_remark_asm, surveyInfo.survey_remark_zo, surveyInfo.flag, '0', getCurrentDate()];
						return DBA.query("INSERT INTO survey_basic_info(surveyId, referenceId, survey_name , survey_status_id , asso_dealer_code , survey_for_dept_code , survey_owner_emp_code , survey_created_date , survey_visited_date , survey_modified_date , survey_month , survey_year , survey_remark , survey_remark_asm , survey_remark_zo , flag, isSynched  , creationDate ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters).then(function(res) {
							   return res;
							}, function (err) {
								 return err;
							});
					}
				}, function (err) {
					 return err;
				});

      });
  }
   self.submitSalesNorms = function(norms) {
	      ////console.log(norms)
		return DBA.query("CREATE TABLE IF NOT EXISTS survey_norms (surveyId numeric, referenceId numeric, alias text, designation text, require_value text, required_value_type text, actual_value text, program_norms text, trained_value text, desig_responce numeric, action_plan text, action_plan_asm text, action_plan_zm text, target_date text, flag numeric,  isSynched numeric ,modificationDate text, creationDate text)").then(function(result) {
		return DBA.query("SELECT * FROM survey_norms WHERE surveyId="+localStorage.getItem("surveyId")+" AND referenceId="+localStorage.getItem("referenceId")).then(function(res) {
			if(res.rows.length>0){
				//update value
				return res;
			}else{
				//add value
				for (var i=0;i<norms.length;i++) {
					var parameters = [localStorage.getItem("surveyId"),localStorage.getItem("referenceId"), norms[i].designationAlias, norms[i].designation, norms[i].required_value, norms[i].required_value_type, norms[i].actual_value, norms[i].action_plan, norms[i].action_plan_asm, norms[i].action_plan_zm, norms[i].target_date, norms[i].flag, '0', getCurrentDate()];
					DBA.query("INSERT INTO survey_norms(surveyId, referenceId, alias, designation , require_value , required_value_type, actual_value , action_plan , action_plan_asm , action_plan_zm , target_date , flag , isSynched , creationDate ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
				}
			}
		}, function (err) {
			 return err;
		});
      });
  }

  self.updateNormsData = function(data) {
    var parameters = [ data.fieldVal, data.alias, data.surveyId, data.referenceId];
    return DBA.query("UPDATE survey_norms SET "+data.column+" = (?) WHERE alias = (?) AND surveyId = (?) AND referenceId = (?)", parameters);
  }
  self.updateSurveyData = function(data) {

    var parameters = [ data.fieldVal, data.isValidated, data.questionId, data.criteriaId, data.surveyId, data.referenceId];
    return DBA.query("UPDATE survey_quetions_answers SET "+data.column+" = (?), isValidated = (?) WHERE questionId = (?) AND criteriaId = (?) AND surveyId = (?) AND referenceId = (?)", parameters);
  }
  self.getSurveyData = function(data) {

    var parameters = [ data.questionId, data.criteriaId, data.surveyId, data.referenceId];
    return DBA.query("SELECT * FROM survey_quetions_answers WHERE questionId = (?) AND criteriaId = (?) AND surveyId = (?) AND referenceId = (?)", parameters);
  }
   self.getSurveyQuestionsData = function(data) {

    var parameters = [ data.criteriaId, data.surveyId, data.referenceId];
    return DBA.query("SELECT * FROM survey_quetions_answers WHERE criteriaId = (?) AND surveyId = (?) AND referenceId = (?)", parameters);
  }
  self.updateSurveyCriteriaStatus = function(data) {

    var parameters = [ data.status, data.criteriaId, data.surveyId, data.referenceId];
    return DBA.query("UPDATE s_criteria_questions SET isValidated = (?) WHERE criteriaId = (?) AND surveyId = (?) AND referenceId = (?)", parameters);
  }

  self.updateRemark = function(data) {
    var parameters = [ data.fieldVal, data.title, data.surveyId, data.referenceId];
    return DBA.query("UPDATE s_remark SET description = (?) WHERE title = (?) AND surveyId = (?) AND referenceId = (?)", parameters);
  }
  self.getSurveyInformation = function(){
	      var parameters = [localStorage.getItem('surveyId'), localStorage.getItem('referenceId')];
			return DBA.query("SELECT * FROM survey_basic_info WHERE surveyId = (?) AND referenceId = (?)", parameters)
			  .then(function(result) {
				return result;
			  });
  }
  self.getNorms = function(){
	      var parameters = [localStorage.getItem('surveyId'), localStorage.getItem('referenceId')];
			return DBA.query("SELECT * FROM survey_norms WHERE surveyId = (?) AND referenceId = (?)", parameters)
			  .then(function(result) {
				return result;
			  });
  }
   self.getCriteria = function(){
	      var parameters = [localStorage.getItem('surveyId'), localStorage.getItem('referenceId')];
			return DBA.query("SELECT scq.criteriaId, scq.criteriaName FROM s_criteria_questions scq WHERE scq.surveyId = (?) AND scq.referenceId = (?) GROUP BY scq.criteriaId", parameters)
			  .then(function(result) {
				return self.getCriteriaQuestions(result.rows);
			  });
  }
  self.getCriteriaQuestions = function(data){
	  var promises = [];
	  for(var i=0;i<data.length;i++){
		  var parameters = [localStorage.getItem('surveyId'), localStorage.getItem('referenceId'), data[i].criteriaId];
			var query = "SELECT scq.criteriaId, scq.isValidated, scq.criteriaName, scq.questionId, scq.questionName, sqa.response, sqa.actionPlan,  sqa.actionPlanDate  FROM s_criteria_questions scq INNER JOIN survey_quetions_answers sqa ON scq.criteriaId=sqa.criteriaId AND scq.questionId=sqa.questionId WHERE scq.surveyId = (?) AND scq.referenceId = (?) AND scq.criteriaId=(?)";
			promises.push(DBA.query(query,parameters));
	  }
	return $q.all(promises);
  }
  self.updateSurveyStatusOnStart = function(data){
	  var parameters = [ data.surveyId, data.referenceId];
	  return DBA.query("SELECT * FROM dealers WHERE surveyId = (?) AND referenceId = (?)", parameters).then(function(res){
			if(res.rows.length>0){
				if(res.rows[0].surveyStatus=="Start"){
					//alert(JSON.stringify(res.rows))
					return DBA.query("UPDATE dealers SET surveyStatus = 'Draft' WHERE surveyId = (?) AND referenceId = (?)", parameters);
				}
			}
	  })
  }
  //offline mode
  self.updateSurveyStatusAfterSubmit = function(data){
	  var parameters = [ data.surveyId, data.referenceId ];
	  return DBA.query("UPDATE dealers SET surveyStatus = 'Submitted_but_unsynced' WHERE surveyId = (?) AND referenceId = (?)", parameters).then(function(res){
		 // //console.log(parameters)
			return DBA.query("UPDATE survey_basic_info SET survey_status_id = "+Submitted_but_unsynced+" WHERE surveyId = (?) AND referenceId = (?)", parameters);
	  });
  }
   //online mode
  self.updateSurveyStatusOnSubmit = function(data){
	if(data.surveyStatusId==1){
		var d_params = [ 'Draft', data.serverSurveyId, data.surveyId, data.referenceId ];
	}else if(data.surveyStatusId==2){
		var d_params = [ 'Submitted', data.serverSurveyId, data.surveyId, data.referenceId ];
	}else if(data.surveyStatusId==3){
		var d_params = [ 'approved_by_asm', data.serverSurveyId, data.surveyId, data.referenceId ];
	}else if(data.surveyStatusId==4){
		var d_params = [ 'approved_by_zo', data.serverSurveyId, data.surveyId, data.referenceId ];
	}

  var params = [ data.surveyStatusId, data.serverSurveyId, data.surveyId, data.referenceId ];
	var parameters = [ data.surveyId, data.referenceId ];
	return DBA.query("UPDATE dealers SET surveyStatus = (?), serverSid = (?) WHERE surveyId = (?) AND referenceId = (?)", d_params).then(function(res){
		return DBA.query("UPDATE survey_basic_info SET isSynched=1, survey_status_id = (?), serverSid = (?) WHERE surveyId = (?) AND referenceId = (?)", params).then(function(res){
			return DBA.query("UPDATE s_criteria_questions SET isSynched=1 WHERE surveyId = (?) AND referenceId = (?)", parameters).then(function(res){
				return DBA.query("UPDATE s_remark SET isSynched=1 WHERE surveyId = (?) AND referenceId = (?)", parameters).then(function(res){
					return DBA.query("UPDATE survey_norms SET isSynched=1 WHERE surveyId = (?) AND referenceId = (?)", parameters).then(function(res){
						return DBA.query("UPDATE survey_quetions_answers SET isSynched=1 WHERE surveyId = (?) AND referenceId = (?)", parameters).then(function(res){

						});
					});
				});
			});
		});
	});
  }
  self.uploadImg = function(imgData) {
      return DBA.query("CREATE TABLE IF NOT EXISTS survey_criteria_attachment (surveyId numeric, referenceId numeric, criteriaId text,fileName text, file text, isSynched numeric ,modificationDate text, creationDate text)").then(function(result) {
  			//add check if data is already exists.
  				return DBA.query("SELECT * FROM survey_criteria_attachment WHERE surveyId="+imgData.surveyId+" AND referenceId="+imgData.referenceId+ " AND criteriaId="+imgData.criteriaId).then(function(res) {
  					////console.log(res)

  					if(res.rows.length>0){
  						//update value
  						var parameters = [imgData.fileName, imgData.file, imgData.surveyId, imgData.referenceId, imgData.criteriaId];
              return DBA.query("UPDATE survey_criteria_attachment SET fileName =(?), file=(?) WHERE surveyId=(?) AND referenceId=(?) AND criteriaId=(?)", parameters).then(function(res) {
                   return res;
                }, function (err) {
                   return err;
                });
  					}else{
  						//add value
  						var parameters = [imgData.surveyId, imgData.referenceId, imgData.criteriaId, imgData.fileName, imgData.file, '0', getCurrentDate()];
  						return DBA.query("INSERT INTO survey_criteria_attachment(surveyId, referenceId, criteriaId, fileName, file, isSynched, creationDate ) VALUES (?,?,?,?,?,?,?)", parameters).then(function(res) {
  							   return res;
  							}, function (err) {
  								 return err;
  							});
  					}
  				}, function (err) {
  					 return err;
  				});

        });

    }
    self.getAttachment = function() {

        return DBA.query("SELECT * FROM survey_criteria_attachment WHERE surveyId="+localStorage.getItem('surveyId')+" AND referenceId="+localStorage.getItem('referenceId')).then(function(res) {
             return res;
        }).catch(function (error) {
          	return error;
        });
    }
  return self;
})

/*
* Author: Anjani Kr. Gupta
* Method: mapping
* Description: dealer mapping
* Date: 24th Aug 2016
*/

.factory('mapping', function($cordovaSQLite, $rootScope, DBA, $q) {
	var self = this;
	self.saveMappingData = function(mappingData, asmEmpCode) {
	//DBA.query("DROP TABLE dealer_mapping");
	////console.log(mappingData)
    return DBA.query("CREATE TABLE IF NOT EXISTS dealer_mapping (refId numeric, asmId numeric, asmEmpCode numeric, dealer_code numeric, dealer_Name text,  dealer_mapped_to_staff_person_code numeric,  emp_name text, dealer_mapped_to_staff_person_department_code text, is_dealer_mapped numeric, isSynched numeric ,modificationDate text, creationDate text)").then(function(result) {
				//check if data is already exists.

				var refId = generateMappingReferenceId(asmEmpCode, mappingData.dealer_code);
				////console.log(refId)
				return DBA.query("SELECT * FROM dealer_mapping WHERE asmEmpCode="+asmEmpCode+" AND refId="+refId+" AND dealer_code="+mappingData.dealer_code).then(function(res) {
					////console.log(res)
					if(res.rows.length>0){
						//update value
 						var parameters = [mappingData.dealer_mapped_to_staff_person_code,  mappingData.emp_name, mappingData.dealer_mapped_to_staff_person_department_code, mappingData.is_dealer_mapped, getCurrentDate(), asmEmpCode, mappingData.dealer_code];
						return DBA.query("UPDATE dealer_mapping SET dealer_mapped_to_staff_person_code=(?), emp_name=(?), dealer_mapped_to_staff_person_department_code=(?), is_dealer_mapped=(?), modificationDate=(?)   WHERE asmEmpCode = (?) AND dealer_code = (?)", parameters).then(function(res) {
							   return res;
							}, function (err) {
								 return err;

							});
					}else{
						//add value
						var parameters = [refId, asmEmpCode, mappingData.dealer_code, mappingData.dealer_Name, mappingData.dealer_mapped_to_staff_person_code, mappingData.emp_name, mappingData.dealer_mapped_to_staff_person_department_code, mappingData.is_dealer_mapped, '0', getCurrentDate()];
						return DBA.query("INSERT INTO dealer_mapping(refId, asmEmpCode, dealer_code, dealer_Name, dealer_mapped_to_staff_person_code, emp_name, dealer_mapped_to_staff_person_department_code, is_dealer_mapped, isSynched, creationDate) VALUES (?,?,?,?,?,?,?,?,?,?)", parameters).then(function(res) {
							   return res;
							}, function (err) {
								 return err;
							});
					}
				}, function (err) {
					 return err;
				});

      });
  }

  self.saveAssociatedTSM = function(data, asmEmpCode) {
	 // DBA.query("DROP TABLE associated_tsm");
	      return DBA.query("CREATE TABLE IF NOT EXISTS associated_tsm (asmId numeric, asmEmpCode numeric, emp_code numeric, emp_name text, isSynched numeric ,modificationDate text, creationDate text)").then(function(result) {
				//check if data is already exists.
				return DBA.query("SELECT * FROM associated_tsm WHERE asmEmpCode="+asmEmpCode).then(function(res) {
					////console.log(res)
					if(res.rows.length>0){
						//update value
						var parameters = [data.emp_code, data.emp_name, getCurrentDate(), asmEmpCode, data.emp_code];
						return DBA.query("UPDATE associated_tsm SET emp_code=(?), emp_name=(?), modificationDate=(?) WHERE asmEmpCode = (?) AND emp_code=(?)", parameters).then(function(res) {
							   return res;
							}, function (err) {
								 return err;
							});
					}else{
						//add value
						var parameters = [asmEmpCode, data.emp_code, data.emp_name, '0', getCurrentDate()];
						return DBA.query("INSERT INTO associated_tsm(asmEmpCode, emp_code, emp_name, isSynched, creationDate) VALUES (?,?,?,?,?)", parameters).then(function(res) {
							   return res;
							}, function (err) {
								 return err;
							});
					}
				}, function (err) {
					 return err;
				});

      });
  }
  self.getMappingData = function(asmEmpCode){
		var query = "SELECT * FROM dealer_mapping WHERE asmEmpCode="+asmEmpCode;
		return DBA.query(query)
		  .then(function(result) {
			return result;
		  });
  }
  self.getAssociatedTSM = function(asmEmpCode){
		var query = "SELECT * FROM associated_tsm WHERE asmEmpCode="+asmEmpCode;
		return DBA.query(query)
		  .then(function(result) {
			return result;
		  });
  }
  self.updateDealerMapping = function(data,asmEmpCode){

		var refId = generateMappingReferenceId(asmEmpCode, data.dealer_code);
		//console.log(refId)
	    var parameters = [ data.dealer_mapped_to_staff_person_code, data.empName, data.dealer_mapped_to_staff_person_department_code, data.is_dealer_mapped, data.isSynched, data.dealer_code, refId, asmEmpCode ];
		  return DBA.query("UPDATE dealer_mapping SET dealer_mapped_to_staff_person_code = (?), emp_name=(?), dealer_mapped_to_staff_person_department_code=(?), is_dealer_mapped = (?), isSynched=(?)  WHERE dealer_code = (?) AND refId = (?) AND asmEmpCode=(?)", parameters).then(function(res){
			 return res;
		  });
  }
	return self;
})
.factory('userProfile', function($cordovaSQLite, $rootScope, DBA, $q) {
	var self = this;
	self.saveUser = function(data) {
		//console.log(data)
		//alert(JSON.stringify(data))
		return DBA.query("CREATE TABLE IF NOT EXISTS profile (user_id numeric,username text, password text, emp_designation text, emp_name text, emp_code numeric, emp_email_id text, emp_department text, emp_primary_contact_no numeric, status numeric, emp_area_office_code numeric, emp_zone_name text, emp_report_to_designation_code numeric, emp_report_to_name text, emp_report_to_person_email_id text, modificationDate text, creationDate text)").then(function(result) {

			return DBA.query("SELECT * FROM profile WHERE username='"+data.username+"' AND emp_code="+data.emp_code).then(function(res) {

				if(res.rows.length>0){
					//update
					 //alert('update')
					var parameters = [data.userId, data.username, data.password, data.emp_code, getCurrentDate(), data.emp_code, data.username];
					return DBA.query("UPDATE profile SET user_id = (?), username =(?), password =(?), emp_code =(?), modificationDate=(?) WHERE emp_code=(?) AND username=(?)", parameters).then(function(res) {

						   return res;

						}, function (err) {
							 return err;
						});
				}else{
					//insert
					 //alert('insert')
					var parameters = [data.userId, data.username, data.password, data.emp_code, data.emp_designation, getCurrentDate()];
					return DBA.query("INSERT INTO profile(user_id, username, password, emp_code, emp_designation, creationDate) VALUES (?,?,?,?,?,?)", parameters).then(function(res) {
						   return res;
						}, function (err) {
							 return err;
						});
				}
			}).catch(function(err){
				//console.log(err)
				return err;
			});
		});
	}

	self.login = function(data) {
		return DBA.query("CREATE TABLE IF NOT EXISTS profile (user_id numeric,username text, password text, emp_designation text, emp_name text, emp_code numeric, emp_email_id text, emp_department text, emp_primary_contact_no numeric, status numeric, emp_area_office_code numeric, emp_zone_name text, emp_report_to_designation_code numeric, emp_report_to_name text, emp_report_to_person_email_id text, modificationDate text, creationDate text)").then(function(result) {

				return DBA.query("SELECT * FROM profile WHERE username='"+data.username+"' AND password='"+data.password+"'").then(function(res) {
					if(res.rows.length>0){
						return res;
					}
				});

			});
	}

	self.updateProfile = function(data) {
	 // DBA.query("DROP TABLE associated_tsm");
	      return DBA.query("CREATE TABLE IF NOT EXISTS profile (username text, password text, emp_designation text, emp_name text, emp_code numeric, emp_email_id text, emp_department text, emp_primary_contact_no numeric, status numeric, emp_area_office_code numeric, emp_zone_name text, emp_report_to_designation_code numeric, emp_report_to_name text, emp_report_to_person_email_id text, modificationDate text, creationDate text)").then(function(result) {
				//check if data is already exists.
				DBA.query("CREATE TABLE IF NOT EXISTS associated_dealers (emp_code numeric, dealer_Name text, dealer_code numeric, dealer_location text, area_office_code numeric, dealer_mapped_to_staff_person_code numeric,  dealer_mapped_to_staff_person_department_code text, is_dealer_mapped numeric, modificationDate text, creationDate text)");
				return DBA.query("SELECT * FROM profile WHERE emp_code="+data.emp_code).then(function(res) {
					////console.log(res)
					if(res.rows.length>0){

						//update value
						var parameters = [data.emp_name, data.emp_code, data.emp_email_id, data.emp_department, data.emp_primary_contact_no, data.status, data.emp_area_office_code, data.emp_zone_name, data.emp_report_to_designation_code, data.emp_report_to_name, data.emp_report_to_person_email_id, getCurrentDate(), data.emp_code];
						return DBA.query("UPDATE profile SET emp_name =(?), emp_code =(?), emp_email_id =(?), emp_department =(?), emp_primary_contact_no =(?), status =(?), emp_area_office_code=(?), emp_zone_name=(?), emp_report_to_designation_code=(?), emp_report_to_name=(?), emp_report_to_person_email_id=(?), modificationDate=(?) WHERE emp_code=(?)", parameters).then(function(res) {
								for (var key in data.dealers) {
										////console.log(data.dealers[key].dealer_Name)
										//}
										//for(var i=0;i<data.dealers.length;i++){
								   ////console.log(data.dealers[key].dealer_code)
									  DBA.query("SELECT * FROM associated_dealers WHERE emp_code="+data.emp_code+ " AND dealer_code="+data.dealers[key].dealer_code).then(function(res) {
										    ////console.log(data.dealers)
										   ////console.log(data.dealers)
										    if(res.rows.length>0){
											 ////console.log(1)
											   var params = [data.dealers[key].dealer_Name, data.dealers[key].dealer_code, data.dealers[key].dealer_location, data.dealers[key].area_office_code, data.dealers[key].dealer_mapped_to_staff_person_code, data.dealers[key].dealer_mapped_to_staff_person_department_code, data.dealers[key].is_dealer_mapped, getCurrentDate(), data.emp_code, data.dealers[key].dealer_code];
												DBA.query("UPDATE associated_dealers SET dealer_Name =(?), dealer_code =(?), dealer_location =(?), area_office_code =(?), dealer_mapped_to_staff_person_code =(?),  dealer_mapped_to_staff_person_department_code =(?), is_dealer_mapped =(?), modificationDate=(?) WHERE emp_code=(?) AND dealer_code=(?)", params);
										   }else{
											   ////console.log(0)
											   var params = [data.emp_code, data.dealers[key].dealer_Name, data.dealers[key].dealer_code, data.dealers[key].dealer_location, data.dealers[key].area_office_code, data.dealers[key].dealer_mapped_to_staff_person_code, data.dealers[key].dealer_mapped_to_staff_person_department_code, data.dealers[key].is_dealer_mapped, getCurrentDate()];
											   DBA.query("INSERT INTO associated_dealers(emp_code, dealer_Name, dealer_code, dealer_location, area_office_code, dealer_mapped_to_staff_person_code,  dealer_mapped_to_staff_person_department_code, is_dealer_mapped, creationDate) VALUES (?,?,?,?,?,?,?,?,?)", params);

										   }
									   });
								   }
							   return res;
							}, function (err) {
								 return err;
							});
					}else{

						//add value
						var parameters = [data.emp_name, data.emp_code, data.emp_email_id, data.emp_department, data.emp_primary_contact_no, data.status, data.emp_area_office_code, data.emp_zone_name, data.emp_report_to_designation_code, data.emp_report_to_name, data.emp_report_to_person_email_id, getCurrentDate()];
						return DBA.query("INSERT INTO profile(emp_name, emp_code, emp_email_id, emp_department, emp_primary_contact_no, status, emp_area_office_code, emp_zone_name, emp_report_to_designation_code, emp_report_to_name, emp_report_to_person_email_id, creationDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", parameters).then(function(res) {

							   for(var i=0;i<data.dealers.length;i++){
								   var params = [data.emp_code, data.dealers[i].dealer_Name, data.dealers[i].dealer_code, data.dealers[i].dealer_location, data.dealers[i].area_office_code, data.dealers[i].dealer_mapped_to_staff_person_code, data.dealers[i].dealer_mapped_to_staff_person_department_code, data.dealers[i].is_dealer_mapped, getCurrentDate()];
								   DBA.query("INSERT INTO associated_dealers(emp_code, dealer_Name, dealer_code, dealer_location, area_office_code, dealer_mapped_to_staff_person_code,  dealer_mapped_to_staff_person_department_code, is_dealer_mapped, creationDate) VALUES (?,?,?,?,?,?,?,?,?)", params);
							   }
							   return res;

							}, function (err) {
								 return err;
							});
					}
				}, function (err) {
					 return err;
				});

      });
	}
	return self;
})
