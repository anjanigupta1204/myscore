// Ionic Starter App
var db;
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngCordova'])
.run(function($ionicPlatform,$cordovaSQLite ) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    try {
            db = $cordovaSQLite.openDB({name:"dealers.db",location:'default'});
            } catch (error) {
                alert(error);
            }

    var x=    $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS dealers ( id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,dealer_name	TEXT, dealer_id	TEXT,	dealer_region	TEXT, modification_date	TEXT,creation_date	TEXT)');
    alert(JSON.stringify(x))
    $cordovaSQLite.execute(db, 'INSERT INTO dealers (dealer_name) VALUES (?)', ["Sid"])
            .then(function(result) {
                $scope.statusMessage = "Message saved successful, cheers!";
				alert($scope.statusMessage)
            }, function(error) {
                $scope.statusMessage = "Error on saving: " + error.message;
				alert($scope.statusMessage)
            })

  });
  $ionicPlatform.onHardwareBackButton(function() {
     event.preventDefault();
     event.stopPropagation();
     alert('going back now');
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

   .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'templates/dashboard.html',
        controller:'dashboardCtrl'
      })

   .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller:'loginCtrl'
  });

  $urlRouterProvider.otherwise('/login');

});
