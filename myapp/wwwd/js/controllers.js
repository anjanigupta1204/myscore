angular.module('starter.controllers', [])
/**
	@Name: loginCtrl
	@Type: Controller
	@Author: Anjani Kr Gupta
	@Date: 4-Jul-2016
*/
.controller('loginCtrl', function ($scope,$http, $state, userAuthenticationService) {



  //Show loader
  $scope.formLoader = false;

  $scope.login = function(){
		var url='http://192.168.1.187:80/drupalApi/drupalapi/user/login';
		var userData={username: $scope.user.email, password: $scope.user.password};
		console.log($scope.user.email);
		console.log($scope.user.password);
		userAuthenticationService.login(url, userData).then(function(data){
			  if(data!=undefined || data != null){
				console.log(data);
				$state.go('dashboard');
			  }else{
				//Close loader
				$scope.formLoader = false;

			  }
			}).catch(function(data){
				alert(data)
		});
  }
})
.controller('dashboardCtrl', function ($scope,$http, $state, userAuthenticationService) {
	console.log("welcome to the dashboard.")
});
