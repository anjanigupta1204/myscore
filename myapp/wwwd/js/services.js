angular.module('starter.services', [])
/**
	@Name: userAuthenticationService
	@Type: Services
	@Author: Anjani Kr Gupta
	@Date: 4-Jul-2016
*/
.service('userAuthenticationService', function ($state, $http, $q) {
  var httpService = {};
	httpService.login = function(_url, _data){
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: _url,
			data: _data,
			headers: {
               'Content-Type': 'application/json'
            },
			cache: false,

		}).success(function(data, status, headers, config){
			deferred.resolve(data);

		})
		.error(function(data, status, headers, config){
			deferred.reject(data);

		});
		return deferred.promise;
	}

	httpService.logout = function(_url){
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: _url,
			headers: {
                'Content-Type': 'application/json'
            },
			cache: false,

		}).success(function(data, status, headers, config){
			deferred.resolve(data);
		})
		.error(function(data, status, headers, config){
			deferred.reject(data);
		});
		return deferred.promise;
	}
  return httpService;
});


